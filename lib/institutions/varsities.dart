import 'package:flutter/material.dart';
import 'package:animated_text_kit/animated_text_kit.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:url_launcher/url_launcher.dart';

void main() => runApp(MaterialApp(
  home: SchoolList(),
));

class SchoolList extends StatefulWidget {
  @override
  _SchoolListState createState() => _SchoolListState();
}

class _SchoolListState extends State<SchoolList> {

  final titles = ['NSFAS','NBT','TVET Colleges','University of Cape Town', 'Stellenbosch University', 'University of Johannesburg', 'WITS', 'University of the Western Cape', 'CPUT', 'University of Pretoria',
  'UKZN', 'University of the Free State', 'TUT', 'NMMU', 'DUT', 'University of Limpopo', 'Dameline', 'City Varsity', 'False Bay College', 'Cape College'];
  final icons = [Icons.monetization_on,Icons.account_balance,Icons.account_balance, Icons.account_balance, Icons.account_balance, Icons.account_balance, Icons.account_balance, Icons.account_balance,Icons.account_balance,
    Icons.account_balance, Icons.account_balance, Icons.account_balance,Icons.account_balance,Icons.account_balance,Icons.account_balance,Icons.account_balance,Icons.account_balance,
    Icons.account_balance,Icons.account_balance,Icons.account_balance,];
  final links = ['http://www.nsfas.co.za','http://www.nbt.ac.za','http://www.fetcolleges.co.za', 'https://www.uct.ac.za', 'https://www.sun.ac.za',  'https://www.uj.ac.za', 'https://www.wits.ac.za', 'https://www.uwc.ac.za',
    'https://www.cput.ac.za',  'https://www.up.ac.za',  'https://www.ukzn.ac.za',  'https://www.ufs.ac.za',  'https://www.tut.ac.za',  'https://www.nmmu.ac.za',  'https://www.dut.ac.za',
    'https://www.ul.ac.za',  'https://www.dameline.co.za',  'https://www.cityvarsity.co.za',  'https://www.falsebaycollege.co.za',  'https://www.cct.edu.za',];

  void launchURL(String url) async {
    var urlLink = url;
    if(await canLaunch(urlLink)){
      await launch(urlLink);
    } else{
      throw 'Could not launch $urlLink';
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        actions: <Widget>[
          SizedBox(
            width: ScreenUtil.getInstance().setHeight(300),
            child: FadeAnimatedTextKit(
              onTap: (){},
              text: ['Apply As Soon As Possible!','Get your marks up!!','Dont procrastinate!!!'],
              textStyle: TextStyle(
                fontSize: ScreenUtil.getInstance().setSp(45),
                fontWeight: FontWeight.bold,
               // fontFamily: 'Caveat-Bold'
              ),
              duration: Duration(seconds: 2),
              textAlign: TextAlign.center,
              alignment: AlignmentDirectional.center,
            ),
          )
        ],
      ),
      body: ListView.builder(
        itemCount: titles.length,
        padding: EdgeInsets.fromLTRB(5,15,5,5),
        itemBuilder: (context, index){
          return Card(
            elevation: 2,
            child: ListTile(
              onTap: (){
                launchURL(links[index]);
              },
              leading: Icon(icons[index]),
              trailing: Icon(Icons.keyboard_arrow_right),
              title: Text(titles[index]),
            ),
          );
        },
      ),
    );
  }
}
