import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:youtube_player_flutter/youtube_player_flutter.dart';

class YoutubeVideos extends StatelessWidget {
  final lifeScience = [
    'https://youtu.be/lZjpGUZfskw',
    'https://youtu.be/kH3GK_0dzKQ',
    'https://youtu.be/B0UwZtqkyls'
  ];
  final mathematics = [
    'https://youtu.be/miTyZbwWCVU' "https://youtu.be/SE5SBTgrwH8",
    "https://youtu.be/bNoJC7R2c0I",
    "https://youtu.be/bNoJC7R2c0I",
    "https://youtu.be/Bx1ipNaKz4Y",
    "https://youtu.be/Bx1ipNaKz4Y",
    "https://youtu.be/Bx1ipNaKz4Y",
    "https://youtu.be/-5kIBPR2Npk",
    "https://youtu.be/-wvF8OQSMx8",
    "https://youtu.be/pxh__ugRKz8"
  ];
  final physics = [
    "https://youtu.be/Fp7D5D8Bqjc",
    "https://youtu.be/pHJQTtEEX4M",
    "https://youtu.be/r-SCyD7f_zI",
    "https://youtu.be/x-Is8oOr7yg",
    "https://youtu.be/-LECEvusk8E",
    "https://youtu.be/OP6RKqSp1Xw",
    "https://youtu.be/dF5lB7gRtcA",
    "https://youtu.be/9fSiy7-JurA",
    "https://youtu.be/n5vjCqnVb6s",
    "https://youtu.be/9blB-uMTIAM",
    "https://youtu.be/wjcPEHhiik8",
    "https://youtu.be/54n1XppP-lA"
  ];
  final economics = [
    "https://youtu.be/myw58Lbrfpw",
    "https://youtu.be/myw58Lbrfpw"
  ];
  final mathsLit = [
    "https://youtu.be/eVzH0winyRY",
    "https://youtu.be/psBwcLUBUvg",
    "https://youtu.be/TEQNpXsh-x8"
  ];
  ScrollController _controller = new ScrollController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Helpfull Youtube Material',
          style: TextStyle(
              fontSize: ScreenUtil.getInstance().setSp(45),
              fontFamily: 'Caveat-Bold'),
        ),
        centerTitle: true,
      ),
      body: ListView(
        controller: _controller,
        shrinkWrap: true,
        scrollDirection: Axis.vertical,
        physics: const AlwaysScrollableScrollPhysics(),
        children: <Widget>[
          MyVideos(
            links: mathematics,
            subject: 'Mathematics',
          ),
          MyVideos(
            links: mathsLit,
            subject: 'Mathematics Literacy',
          ),
          MyVideos(
            links: physics,
            subject: 'Physical Sciences',
          ),
          MyVideos(
            links: lifeScience,
            subject: 'Life Sciences',
          ),
          MyVideos(
            links: economics,
            subject: 'Economics',
          ),
        ],
      ),
    );
  }
}

class MyVideos extends StatefulWidget {
  final links;
  final String subject;

  MyVideos({this.links, this.subject});

  @override
  _MyVideosState createState() => _MyVideosState();
}

class _MyVideosState extends State<MyVideos> {
  ScrollController _controller = new ScrollController();
  @override
  Widget build(BuildContext context) {
    return ExpansionTile(
      title: Text('${widget.subject}'),
      children: <Widget>[
        ListView.builder(
          scrollDirection: Axis.vertical,
          shrinkWrap: true,
          controller: _controller,
          physics: ScrollPhysics(),
          padding: EdgeInsets.fromLTRB(40.0, 10.0, 40.0, 40.0),
          itemCount: widget.links.length,
          itemBuilder: (context, index) {
            return Card(
              margin: new EdgeInsets.symmetric(horizontal: 10.0, vertical: 6.0),
              borderOnForeground: true,
              elevation: 7.0,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Container(
                  child: YoutubePlayer(
                    controller: YoutubePlayerController(
                        initialVideoId:
                            YoutubePlayer.convertUrlToId(widget.links[index]),
                        flags: YoutubePlayerFlags(
                            mute: false,
                            controlsVisibleAtStart: true,
                            autoPlay: false)),
                    showVideoProgressIndicator: true,
                    progressIndicatorColor: Colors.amber,
                    progressColors: ProgressBarColors(
                        playedColor: Colors.amber,
                        handleColor: Colors.amberAccent),
                    bottomActions: <Widget>[
                      CurrentPosition(),
                      ProgressBar(
                        isExpanded: true,
                      ),
                      PlayPauseButton(),
                      FullScreenButton(),
                      CurrentPosition(),
                      RemainingDuration(),
                    ],
                  ),
                ),
              ),
            );
          },
        )
      ],
    );
  }
}
