import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:intsika/e_learning/youtube_page.dart';
import 'package:url_launcher/url_launcher.dart';

void main() => runApp(MaterialApp(
  home: ResourcesList(),
));

class ResourcesList extends StatefulWidget {
  @override
  _ResourcesListState createState() => _ResourcesListState();
}

class _ResourcesListState extends State<ResourcesList> {

  void launchURL(String url) async {
    var urlLink = url;
    if(await canLaunch(urlLink)){
      await launch(urlLink);
    } else{
      throw 'Could not launch $urlLink';
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Online Resources',
          style: TextStyle(
            fontFamily: 'Caveat-Bold',
            fontSize: ScreenUtil.getInstance().setSp(45),
          ),
        ),
        centerTitle: true,
      ),
      body: ListView(
        padding: EdgeInsets.only(top: ScreenUtil.getInstance().setHeight(20)),
        children: <Widget>[
          Card(
            child: ListTile(
              onTap: (){
                Navigator.push(context, MaterialPageRoute(builder: (context) => YoutubeVideos()));
              },
              title: Text('Youtube Videos'),
              leading: Icon(Icons.ondemand_video, color: Colors.red,),
              trailing: Icon(Icons.keyboard_arrow_right),
            ),
          ),
          Card(
            child: ListTile(
              onTap: (){
                launchURL("https://www.khanacademy.org");
              },
              title: Text('Khan Academy Site'),
              leading: Icon(Icons.favorite, color: Colors.green,),
              trailing: Icon(Icons.keyboard_arrow_right),
            ),
          ),
          Card(
            child: ListTile(
              onTap: (){
                launchURL("http://www0.sun.ac.za/school/");
              },
              title: Text('Telematics Site'),
              leading: Icon(Icons.tv, color: Colors.blueAccent,),
              trailing: Icon(Icons.keyboard_arrow_right),
            ),
          ),
        ],
      ),
    );
  }
}
