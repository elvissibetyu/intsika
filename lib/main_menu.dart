import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:intsika/about_app.dart';
import 'package:intsika/chatroom/chat.dart';
import 'package:intsika/e_learning/e_resources.dart';
import 'package:intsika/e_textbooks/textbooks.dart';
import 'package:intsika/institutions/varsities.dart';
import 'package:intsika/login.dart';
import 'package:intsika/notifications/notificationList.dart';
import 'package:intsika/profile.dart';
import 'subjects_pages/subjects.dart';
import 'tutoring/tutors.dart';
import 'e_textbooks/textbooks.dart';
import 'package:animated_text_kit/animated_text_kit.dart';

void main() =>
  runApp(MaterialApp(
    home: Menu(),
    debugShowCheckedModeBanner: false,
  ));

class Menu extends StatefulWidget {
  @override
  _MenuState createState() => _MenuState();
}

class _MenuState extends State<Menu> {

  final FirebaseAuth _auth = FirebaseAuth.instance;
  FirebaseUser user;

  void getUser() async {
    user = await _auth.currentUser();
    setState(() {

    });
  }

  @override
  void initState() {
    getUser();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    ScreenUtil.instance = ScreenUtil(width: 800, height: 1400, allowFontScaling: true)..init(context);
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Intsika',
          style: TextStyle(
            fontFamily: 'Caveat-Bold',
            fontSize: ScreenUtil.getInstance().setSp(60)
          ),
        ),
        centerTitle: true,
      ),
      backgroundColor: Colors.blue[50],
      drawer: Drawer(
        child: ListView(
          children: <Widget>[
            UserAccountsDrawerHeader(
              /*currentAccountPicture: CircleAvatar(
                child: Image.,
              ),*/
              currentAccountPicture: CircleAvatar(
                backgroundImage: AssetImage('assets/instika_logo_circular.PNG'),
                radius: ScreenUtil.getInstance().setHeight(90),
              ),
              accountName: Text(user.displayName != null  ? user.displayName : 'Full Name', style: TextStyle(color: Colors.black),),
              accountEmail: Text(user.email != null  ? user.email : 'Email', style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold),),
            ),
            InkWell(
              onTap: () async {
                Navigator.push(context, MaterialPageRoute(builder: (context) => UserProfile()));
              },
              child: ListTile(
                leading: Icon(
                    Icons.person
                ),
                title: Text('Profile'),
              ),
            ),
            InkWell(
              onTap: (){
                //if(user != null) {
                  String email = user.email;
                  _auth.sendPasswordResetEmail(email: user.email);

                  showDialog(
                    context: context,
                    builder: (context){
                      return AlertDialog(
                        content: Text('An email was sent to $email, go to your emails to reset your password.'),
                        title: Text('Email Sent'),
                        actions: <Widget>[
                          FlatButton(
                            onPressed: (){
                              Navigator.of(context).pop();
                            },
                            child: Text(
                              'Close'
                            ),
                          ),
                        ],
                        );
                    }
                  );
                //}
              },
              child: ListTile(
                leading: Icon(
                    Icons.vpn_key
                ),
                title: Text('Change Password'),
              ),
            ),
            InkWell(
              onTap: () async {
                Navigator.push(context, MaterialPageRoute(builder: (context) => AboutApp()));
              },
              child: ListTile(
                leading: Icon(
                    Icons.info_outline
                ),
                title: Text('About App'),
              ),
            ),
            InkWell(
              onTap: (){
                _auth.signOut();
                Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => LogIn()));
              },
              child: ListTile(
                leading: Icon(
                    Icons.power_settings_new
                ),
                title: Text('Logout'),
              ),
            ),
          ],
        ),
      ),

      body: MyGrid(),

      );

  }
}

class MyGrid extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Padding(
          padding:  EdgeInsets.only(left: ScreenUtil.getInstance().setHeight(180)),
          child: SizedBox(
            width: ScreenUtil.getInstance().setHeight(250),
            child: TypewriterAnimatedTextKit(
              isRepeatingAnimation: true,
              onTap: (){},
              text: [
                'Don\'t let what you cannot do interfer with what you can do.',
                'Dreams don\'t work unless you do.',
                'To teach is to learn twice.',
                'Intsika will be there, in your times of academic need'
              ],
              textStyle: TextStyle(fontFamily: 'Caveat-Bold', fontSize: ScreenUtil.getInstance().setSp(50)),
              textAlign: TextAlign.center,
              alignment: AlignmentDirectional.topCenter,
            ),
          ),
        ),
        Container(
        padding: EdgeInsets.fromLTRB(ScreenUtil.getInstance().setHeight(35),ScreenUtil.getInstance().setHeight(300), ScreenUtil.getInstance().setHeight(35), ScreenUtil.getInstance().setHeight(35)),
        child: GridView.count(
          crossAxisCount: 3,
          children: <Widget>[
            MyMenu(text: 'Chatroom', icon: Icons.chat, color: Colors.blue[600],),
            MyMenu(text: 'Notifications', icon: Icons.notification_important, color: Colors.red[200],),
            MyMenu(text: 'Subjects', icon: Icons.school, color: Colors.orange,),
            MyMenu(text: 'Tutoring', icon: Icons.local_library, color: Colors.green[800],),
            MyMenu(text: 'Universities/Colleges', icon: Icons.account_balance, color: Colors.brown[400],),
            MyMenu(text: 'Online Resources', icon: Icons.ondemand_video, color: Colors.red[800],),
            MyMenu(text: 'Past Papers', icon: Icons.library_books, color: Colors.black12,),
          ],
        ),
      ),
      ],
    );
  }
}


class MyMenu extends StatefulWidget {

  final String text;
  final Color color;
  final IconData icon;

  MyMenu({this.text, this.icon, this.color});

  @override
  _MyMenuState createState() => _MyMenuState();
}

class _MyMenuState extends State<MyMenu> {

  get onClicked{
    return widget.text;
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Card(
      margin: EdgeInsets.all(ScreenUtil.getInstance().setHeight(10)),
      child: InkWell(
        onTap: (){
          String menuItem = onClicked;
          if(menuItem == 'Subjects'){
            Navigator.push(context,
              MaterialPageRoute(builder: (context) => ListPage()),
            );
          }
          else if(menuItem == 'Tutoring'){
            Navigator.push(context,
              MaterialPageRoute(builder: (context) => TutorList())
            );
          }
          else if(menuItem == 'Past Papers'){
            Navigator.push(context,
            MaterialPageRoute(builder: (context) => TextBookList()));
          }
          else if(menuItem == 'Chatroom'){
            Navigator.push(context, MaterialPageRoute(builder: (context) => ChatScreen()));
          }
          else if(menuItem == 'Universities/Colleges'){
            Navigator.push(context, MaterialPageRoute(builder: (context) => SchoolList()));
          }
          else if(menuItem == 'Online Resources'){
            Navigator.push(context, MaterialPageRoute(builder: (context) => ResourcesList()));
          }
          else if(menuItem == 'Notifications'){
            Navigator.push(context, MaterialPageRoute(builder: (context) => NotificationItems()));
          }
        },
        splashColor: Colors.blue,
        child: Center(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Icon(
                widget.icon,
                size: ScreenUtil.getInstance().setHeight(130),
                color: widget.color,
              ),
              Text(
                widget.text,
                style: TextStyle(
                    fontSize: ScreenUtil.getInstance().setSp(25)
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
