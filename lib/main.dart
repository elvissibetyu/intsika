import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'dart:math';
import 'dart:async';
import 'package:intsika/login.dart';
import 'package:intsika/main_menu.dart';

void main() => runApp(MaterialApp(
  home: Loader(),
  debugShowCheckedModeBanner: false,
));

class Loader extends StatefulWidget {
  @override
  _LoaderState createState() => _LoaderState();
}

class _LoaderState extends State<Loader> with TickerProviderStateMixin {

  AnimationController controller;
  Animation<double> animation_rotation;
  Animation<double> radius_in;
  Animation<double> radius_out;
  final double initialradius = 30.0;
  double radius = 0.0;

  @override
  void initState() {
    super.initState();
    controller = AnimationController(vsync: this, duration: Duration(seconds: 1));
    radius_in =Tween<double>(
      begin: 1.0,
      end: 0.0,
    ).animate(CurvedAnimation(parent: controller, curve: Interval(0.75, 1.0, curve: Curves.elasticIn)));

    controller = AnimationController(vsync: this, duration: Duration(seconds: 3));

    radius_out =Tween<double>(
      begin: 0.0,
      end: 1.0,
    ).animate(CurvedAnimation(parent: controller, curve: Interval(0.0, 0.25, curve: Curves.elasticOut)));

    animation_rotation = Tween<double>(
      begin: 0.0,
      end: 1.0,
    ).animate(CurvedAnimation(parent: controller, curve: Interval(0.0, 1.0, curve: Curves.linear)));



    controller.addListener((){

      setState(() {
        if(controller.value >= 0.75 && controller.value <= 1.0){
          radius = initialradius * radius_in.value;
        }
        else if(controller.value >= 0.0 && controller.value <= 0.25){
          radius = initialradius * radius_out.value;
        }
      });

    });
    controller.repeat();

    Future.delayed(const Duration(milliseconds: 7000), () {
      controller.dispose();
      setState(()  {
        checkUser();
      });
    });
  }

  void checkUser() async{
    FirebaseUser user = await FirebaseAuth.instance.currentUser();
    if(user != null){
      Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => Menu()));
    }
    else{
      Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => LogIn()));
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 100.0,
      height: 100.0,
      child: Center(
        child: RotationTransition(
          turns: animation_rotation,
          child: Stack(
            children: <Widget>[
              Dot(
                color: Colors.black12,
                radius: 30.0,
              ),
              Transform.translate(
                offset: Offset(radius * cos(pi/4), radius * sin(pi/4)),
                child: Dot(
                  color: Colors.blue,
                  radius: 5.0,
                ),
              ),
              Transform.translate(
                offset: Offset(radius * cos(2*pi/4), radius * sin(2*pi/4)),
                child: Dot(
                  color: Colors.black,
                  radius: 5.0,
                ),
              ),
              Transform.translate(
                offset: Offset(radius * cos(3*pi/4), radius * sin(3*pi/4)),
                child: Dot(
                  color: Colors.grey,
                  radius: 5.0,
                ),
              ),
              Transform.translate(
                offset: Offset(radius * cos(4*pi/4), radius * sin(4*pi/4)),
                child: Dot(
                  color: Colors.green,
                  radius: 5.0,
                ),
              ),
              Transform.translate(
                offset: Offset(radius * cos(5*pi/4), radius * sin(5*pi/4)),
                child: Dot(
                  color: Colors.lightGreen,
                  radius: 5.0,
                ),
              ),
              Transform.translate(
                offset: Offset(radius * cos(6*pi/4), radius * sin(6*pi/4)),
                child: Dot(
                  color: Colors.lime,
                  radius: 5.0,
                ),
              ),
              Transform.translate(
                offset: Offset(radius * cos(7*pi/4), radius * sin(7*pi/4)),
                child: Dot(
                  color: Colors.brown,
                  radius: 5.0,
                ),
              ),
              Transform.translate(
                offset: Offset(radius * cos(8*pi/4), radius * sin(8*pi/4)),
                child: Dot(
                  color: Colors.red,
                  radius: 5.0,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class Dot extends StatelessWidget {

  final double radius;
  final Color color;

  Dot({this.color, this.radius});

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        width: this.radius,
        height: this.radius,
        decoration: BoxDecoration(
            color: this.color,
            shape: BoxShape.circle
        ),
      ),
    );
  }
}
