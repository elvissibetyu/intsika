import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_pdfview/flutter_pdfview.dart';
import 'package:http/http.dart' as http;
import 'package:path_provider/path_provider.dart';


class TextBookList extends StatefulWidget {
  @override
  _TextBookListState createState() => _TextBookListState();
}

class _TextBookListState extends State<TextBookList> {

    List<String> paths = ['','','','','','','','','','','','','','','','','','','','','','','','',];

  @override
  void initState() {
    super.initState();

    getFileFromAsset('assets/June_2014_Practice_Exam_Paper_1_Grade_12_Mathematics.pdf', 'SharpExamJune').then((f) {
      setState(() {
        paths[0] = f.path;
      });
    });
    getFileFromAsset('assets/June_2014_Memorandum_Practice_Exam_Paper_1_Grade_12_Mathematics.pdf', 'SharpExamMemoJune').then((f) {
      setState(() {
        paths[1] = f.path;
      });
    });

    getFileFromAsset('assets/2016-SEPT-P1-QP-MEMO.pdf', '2016-SEPT-P1-QP-MEMO').then((f) {
      setState(() {
        paths[2] = f.path;
      });
    });
    getFileFromAsset('assets/2016-MATHS-QP-MEMO.pdf', '2016CommonTest').then((f) {
      setState(() {
        paths[3] = f.path;
      });
    });
    getFileFromAsset('assets/2016-SEPT-P2-QP-MEMO.pdf', '2016SeptemberMaths').then((f) {
      setState(() {
        paths[4] = f.path;
      });
    });
    getFileFromAsset('assets/2015-Sewptember-MATHS-P2-QP-MEMO.pdf', '2015SeptemberMaths').then((f) {
      setState(() {
        paths[5] = f.path;
      });
    });
    getFileFromAsset('assets/maths_lit_p1.pdf', '2014MathsLit').then((f) {
      setState(() {
        paths[6] = f.path;
      });
    });
    getFileFromAsset('assets/maths_lit_p1_memo.pdf', '2014MathsLitMemo').then((f) {
      setState(() {
        paths[7] = f.path;
      });
    });
    getFileFromAsset('assets/maths_lit_p2.pdf', '2014MathsLitP2').then((f) {
      setState(() {
        paths[8] = f.path;
      });
    });
    getFileFromAsset('assets/maths_lit_p2_memo.pdf', '2014MathsLitP2Memo').then((f) {
      setState(() {
        paths[9] = f.path;
      });
    });
    getFileFromAsset('assets/2018Phy1.pdf', '2018PhyP1withMemo').then((f) {
      setState(() {
        paths[10] = f.path;
      });
    });
    getFileFromAsset('assets/2018Phy2.pdf', '2018PhyP2withMemo').then((f) {
      setState(() {
        paths[11] = f.path;
      });
    });
    getFileFromAsset('assets/2016Phy1.pdf', '2016PhyP1withMemo').then((f) {
      setState(() {
        paths[12] = f.path;
      });
    });
    getFileFromAsset('assets/2016Phy2.pdf', '2016PhyP1withMemo').then((f) {
      setState(() {
        paths[13] = f.path;
      });
    });
    getFileFromAsset('assets/2017LSCPaper1.pdf', '2017LSCP2withMemo').then((f) {
      setState(() {
        paths[14] = f.path;
      });
    });
    getFileFromAsset('assets/2017LSCPaper2.pdf', '2017LSCP2withMemo').then((f) {
      setState(() {
        paths[15] = f.path;
      });
    });
    getFileFromAsset('assets/2016LSCPaper1.pdf', '2016LSCP1withMemo').then((f) {
      setState(() {
        paths[16] = f.path;
      });
    });
    getFileFromAsset('assets/2016LSCPaper2.pdf', '2016LSCP2withMemo').then((f) {
      setState(() {
        paths[17] = f.path;
      });
    });
    getFileFromAsset('assets/2013EcosP1.pdf', 'Economics2013P1').then((f) {
      setState(() {
        paths[18] = f.path;
      });
    });
    getFileFromAsset('assets/EcosP1Memo.pdf', 'EcosP1Memo').then((f) {
      setState(() {
        paths[19] = f.path;
      });
    });
    getFileFromAsset('assets/Economics-2014.pdf', 'EcosP1P2').then((f) {
      setState(() {
        paths[20] = f.path;
      });
    });
  }

  Future<File> getFileFromAsset(String asset, String fileName) async {
    try {
      var data = await rootBundle.load(asset);
      var bytes = data.buffer.asUint8List();
      var dir = await getApplicationDocumentsDirectory();
      File file = File('${dir.path}/$fileName');
      File assetFile = await file.writeAsBytes(bytes);
      return assetFile;
    } catch (e) {
      throw Exception('Could not load url file' + e.toString());
    }
  }

  Future<File> getFileFromUrl(String url) async {
    try {
      var data = await http.get(url);
      var bytes = data.bodyBytes;
      var dir = await getApplicationDocumentsDirectory();
      File file = File('${dir.path}/instika_textbook.pdf');
      File urlFile = await file.writeAsBytes(bytes);
      return urlFile;
    } catch (e) {
      throw Exception('Could not load url file' + e.toString());
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Click any paper and read',
          style: TextStyle(
            fontFamily: 'Caveat-Bold',
            fontSize: ScreenUtil.getInstance().setSp(40),
          ),
        ),
      ),
      body: Container(
        padding: EdgeInsets.only(
            left: ScreenUtil.getInstance().setHeight(15),
            top: ScreenUtil.getInstance().setHeight(30),
            right: ScreenUtil.getInstance().setHeight(15)),
        child: ListView(
          children: <Widget>[
            ExpansionTile(
              title: Text('Mathematics'),
              children: <Widget>[
                MakeListTile(
                  text: 'SHARP June Paper 1 - Maths',
                  color: Colors.red[400],
                  url: paths[0],
                ),
                MakeListTile(
                  text: 'SHARP June Paper 1 Memo - Maths',
                  color: Colors.red[400],
                  url: paths[1],
                ),
                MakeListTile(
                  text: '2016-KZN-SEPT-P1-Question Paper and Memo',
                  color: Colors.red[400],
                  url: paths[2],
                ),
                MakeListTile(
                  text: '2016 Common Test - Question Paper & Memo',
                  color: Colors.red[400],
                  url: paths[3],
                ),
                MakeListTile(
                  text: '2016-KZN-MATHS-P2-Question & Memo',
                  color: Colors.red[400],
                  url: paths[4],
                ),
                MakeListTile(
                  text: '2015-Sept-KZN-MATHS-P2-Question & MEMO',
                  color: Colors.red[400],
                  url: paths[5],
                ),
              ],
            ),
            ExpansionTile(
              title: Text('Mathematics Literacy'),
              children: <Widget>[
                MakeListTile(
                  text: '2014 Paper 1 - Maths Lit',
                  color: Colors.red[400],
                  url: paths[6],
                ),
                MakeListTile(
                  text: '2014 Paper 1 - Maths Lit Memo',
                  color: Colors.red[400],
                  url: paths[7],
                ),
                MakeListTile(
                  text: '2014 Paper 2 - Maths Lit',
                  color: Colors.red[400],
                  url: paths[8],
                ),
                MakeListTile(
                  text: '2014 Paper 2 - Maths Lit Memo',
                  color: Colors.red[400],
                  url: paths[9],
                ),
              ],
            ),
            ExpansionTile(
              title: Text('Physical Sciences'),
              children: <Widget>[
                MakeListTile(
                  text: 'KZN Sept Paper 1 2018 with Memo',
                  color: Colors.red[400],
                  url: paths[10],
                ),
                MakeListTile(
                  text: 'KZN Sept Paper 2 2018 with Memo',
                  color: Colors.red[400],
                  url: paths[11],
                ),
                MakeListTile(
                  text: 'KZN Sept Paper 1 2016 with Memo',
                  color: Colors.red[400],
                  url: paths[12],
                ),
                MakeListTile(
                  text: 'KZN Sept Paper 2 2016 with Memo',
                  color: Colors.red[400],
                  url: paths[13],
                ),
              ],
            ),
            ExpansionTile(
              title: Text('Life Sciences'),
              children: <Widget>[
                MakeListTile(
                  text: 'KZN LSC Paper 1 2017 with Memo',
                  color: Colors.red[400],
                  url: paths[14],
                ),
                MakeListTile(
                  text: 'KZN LSC Paper 2 2017 with Memo',
                  color: Colors.red[400],
                  url: paths[15],
                ),
                MakeListTile(
                  text: 'KZN LSC Paper 1 2016 with Memo',
                  color: Colors.red[400],
                  url: paths[16],
                ),
                MakeListTile(
                  text: 'KZN LSC Paper 2 2016 with Memo',
                  color: Colors.red[400],
                  url: paths[17],
                ),
              ],
            ),
            ExpansionTile(

              title: Text('Economics'),
              children: <Widget>[
                MakeListTile(
                  text: 'Economics 2013',
                  color: Colors.red[400],
                  url: paths[18],
                ),
                MakeListTile(
                  text: 'Economics 2013 Memo',
                  color: Colors.red[400],
                  url: paths[19],
                ),
                MakeListTile(
                  text: 'Economics 2014 P1 and P2 with Memos - all combined',
                  color: Colors.red[400],
                  url: paths[20],
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}

class MakeListTile extends StatelessWidget {
  final String text;
  final Color color;
  final String url;

  getTapped() {
    return text;
  }

  MakeListTile({this.text, this.color, this.url});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        String tapped = getTapped();
        if (tapped == 'SHARP June Paper 1 - Maths') {
          if (url != null) {
            Navigator.push(context, MaterialPageRoute(builder: (context) => PDFViewPage(path: url,)));
          }
        } else if (tapped == 'SHARP June Paper 1 Memo - Maths') {
          Navigator.push(context, MaterialPageRoute(builder: (context) => PDFViewPage(path: url,)));
        } else if (tapped == '2016-KZN-SEPT-P1-Question Paper and Memo') {
          Navigator.push(context, MaterialPageRoute(builder: (context) => PDFViewPage(path: url,)));
        } else if (tapped == '2016 Common Test - Question Paper & Memo') {
          Navigator.push(context, MaterialPageRoute(builder: (context) => PDFViewPage(path: url,)));
        } else if (tapped == '2016-KZN-MATHS-P2-Question & Memo') {
          Navigator.push(context, MaterialPageRoute(builder: (context) => PDFViewPage(path: url,)));
        } else if (tapped == '2015-Sept-KZN-MATHS-P2-Question & MEMO') {
          Navigator.push(context, MaterialPageRoute(builder: (context) => PDFViewPage(path: url,)));
        } else if (tapped == '2014 Paper 1 - Maths Lit') {
          Navigator.push(context, MaterialPageRoute(builder: (context) => PDFViewPage(path: url,)));
        } else if (tapped == '2014 Paper 1 - Maths Lit Memo') {
          Navigator.push(context, MaterialPageRoute(builder: (context) => PDFViewPage(path: url,)));}
        else if (tapped == '2014 Paper 2 - Maths Lit') {
          Navigator.push(context, MaterialPageRoute(builder: (context) => PDFViewPage(path: url,)));}
        else if (tapped == '2014 Paper 2 - Maths Lit Memo') {
          Navigator.push(context, MaterialPageRoute(builder: (context) => PDFViewPage(path: url,))); //KZN Sept Paper 2 2016 with Memo
        }
        else if (tapped == 'KZN Sept Paper 1 2018 with Memo') {
          Navigator.push(context, MaterialPageRoute(builder: (context) => PDFViewPage(path: url,)));
        }
        else if (tapped == 'KZN Sept Paper 2 2018 with Memo') {
          Navigator.push(context, MaterialPageRoute(builder: (context) => PDFViewPage(path: url,)));
        }
        else if (tapped == 'KZN Sept Paper 1 2016 with Memo') {
          Navigator.push(context, MaterialPageRoute(builder: (context) => PDFViewPage(path: url,)));
        }
        else if (tapped == 'KZN Sept Paper 2 2016 with Memo') {
          Navigator.push(context, MaterialPageRoute(builder: (context) => PDFViewPage(path: url,)));
        }
        else if (tapped == 'KZN LSC Paper 1 2017 with Memo') {
          Navigator.push(context, MaterialPageRoute(builder: (context) => PDFViewPage(path: url,)));
        }
        else if (tapped == 'KZN LSC Paper 2 2017 with Memo') {
          Navigator.push(context, MaterialPageRoute(builder: (context) => PDFViewPage(path: url,)));
        }
        else if (tapped == 'KZN LSC Paper 1 2016 with Memo') {
          Navigator.push(context, MaterialPageRoute(builder: (context) => PDFViewPage(path: url,)));
        }
        else if (tapped == 'KZN LSC Paper 2 2016 with Memo') {
          Navigator.push(context, MaterialPageRoute(builder: (context) => PDFViewPage(path: url,)));
        }
        else if (tapped == 'Economics 2013') {
          Navigator.push(context, MaterialPageRoute(builder: (context) => PDFViewPage(path: url,)));
        }
        else if (tapped == 'Economics 2013 Memo') {
          Navigator.push(context, MaterialPageRoute(builder: (context) => PDFViewPage(path: url,)));
        }
        else if (tapped == 'Economics 2014 P1 and P2 with Memos - all combined') {
          Navigator.push(context, MaterialPageRoute(builder: (context) => PDFViewPage(path: url,)));
        }
      },
      child: Card(
        elevation: 8.0,
        margin: new EdgeInsets.symmetric(horizontal: 10.0, vertical: 6.0),
        child: Container(
          decoration: BoxDecoration(color: color),
          child: ListTile(
              contentPadding:
                  EdgeInsets.symmetric(horizontal: 20.0, vertical: 10.0),
              leading: Container(
                padding: EdgeInsets.only(right: 12.0),
                decoration: new BoxDecoration(
                    border: new Border(
                        right:
                            new BorderSide(width: 1.0, color: Colors.white24))),
                child: Icon(Icons.library_books, color: Colors.white),
              ),
              title: Text(
                text,
                style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                    fontSize: ScreenUtil.getInstance().setSp(30)),
              ),
// subtitle: Text("Intermediate", style: TextStyle(color: Colors.white)),
              trailing: Icon(Icons.keyboard_arrow_right,
                  color: Colors.white, size: 30.0)),
        ),
      ),
    );
  }
}

class PDFViewPage extends StatefulWidget {
  final String path;

  PDFViewPage({Key key, this.path}): super(key: key);

  @override
  _PDFViewPageState createState() => _PDFViewPageState();
}

class _PDFViewPageState extends State<PDFViewPage> {
  bool isReady = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          '(Swipe Left Or Right)',
          style: TextStyle(
            fontFamily: 'Caveat-Bold',
            color: Colors.white,
            fontSize: ScreenUtil.getInstance().setSp(45)
          ),
        ),
        centerTitle: true,
      ),
      body:
         Stack(
          children: <Widget>[
            PDFView(
              filePath: widget.path,
              autoSpacing: true,
              enableSwipe: true,
              //nightMode: true,
              pageSnap: true,
              swipeHorizontal: true,
              onRender: (_pages){
                setState(() {
                  isReady = true;
                });
              },
              onPageChanged: (int page, int total){
                setState(() {

                });
              },
            ),
            //!isReady?  Center(child: CircularProgressIndicator(),):Offstage
          ],
        ),

      );
  }
}
