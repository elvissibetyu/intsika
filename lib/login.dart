import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:intsika/main_menu.dart';
import 'package:intsika/signup.dart';

void main() => runApp(MaterialApp(
      home: LogIn(),
      debugShowCheckedModeBanner: false,
    ));

class LogIn extends StatefulWidget {
  @override
  _LogInState createState() => _LogInState();
}

class _LogInState extends State<LogIn> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  String username;
  String password;

  final FirebaseAuth _auth = FirebaseAuth.instance;

  Future<String> signIn(String email, String password) async {
    AuthResult user = await _auth.signInWithEmailAndPassword(
        email: email, password: password);

    Navigator.pushReplacement(
      context,
      MaterialPageRoute(builder: (context) => Menu()),
    );
  }

  TextEditingController usernameController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  @override
  void dispose() {
    usernameController.dispose();
    passwordController.dispose();
    emailController.dispose();
    super.dispose();
  }

  bool _isSelected = true;

  void _radio() {
    setState(() {
      _isSelected = !_isSelected;
    });
  }

  Widget radioButton(bool isSelected) => Container(
        width: 16.0,
        height: 16.0,
        padding: EdgeInsets.all(2.0),
        decoration: BoxDecoration(
            shape: BoxShape.circle,
            border: Border.all(width: 2.0, color: Colors.black)),
        child: isSelected
            ? Container(
                width: double.infinity,
                height: double.infinity,
                decoration:
                    BoxDecoration(shape: BoxShape.circle, color: Colors.black),
              )
            : Container(),
      );

  Widget horizontalLine() => Padding(
        padding: EdgeInsets.symmetric(horizontal: 1.0),
        child: Container(
          width: ScreenUtil.getInstance().setWidth(60),
          height: 1.0,
          color: Colors.black26.withOpacity(.8),
        ),
      );

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    ScreenUtil.instance = ScreenUtil.getInstance()..init(context);
    ScreenUtil.instance =
        ScreenUtil(width: 750, height: 1334, allowFontScaling: true);
    return new Scaffold(
      key: _scaffoldKey,
      backgroundColor: Colors.white,
      resizeToAvoidBottomPadding: true,
      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.only(top: 20.0),
                  child: Image.asset('assets/Intsika_Logo.PNG'),
                ),
              ],
            ),
          ),
          SingleChildScrollView(
            padding: EdgeInsets.only(left: 20.0, right: 20.0, top: 60.0),
            child: Column(
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Image.asset(
                      'assets/instika_logo_circular.PNG',
                      width: ScreenUtil.getInstance().setWidth(110),
                      height: ScreenUtil.getInstance().setHeight(110),
                    ),
                    Text("Welcome",
                        style: TextStyle(
                            fontFamily: "Caveat-Bold",
                            fontSize: ScreenUtil.getInstance().setSp(42),
                            letterSpacing: .6,
                            color: Colors.brown[400],
                            fontWeight: FontWeight.bold))
                  ],
                ),
                SizedBox(
                  height: ScreenUtil.getInstance().setHeight(300),
                ),
                Container(
                  width: double.infinity,
                  height: ScreenUtil.getInstance().setHeight(590),
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(8.0),
                      boxShadow: [
                        BoxShadow(
                            color: Colors.black12,
                            offset: Offset(0.0, 15.0),
                            blurRadius: 15.0),
                        BoxShadow(
                            color: Colors.black12,
                            offset: Offset(0.0, -10.0),
                            blurRadius: 10.0)
                      ]),
                  child: Padding(
                    padding:
                        EdgeInsets.only(left: 16.0, right: 16.0, top: 16.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          'Login',
                          style: TextStyle(
                            fontFamily: "Caveat-Bold",
                            fontSize: ScreenUtil.getInstance().setSp(50),
                            letterSpacing: .6,
                          ),
                        ),
                        SizedBox(
                          height: ScreenUtil.getInstance().setHeight(30),
                        ),
                        Text(
                          'Username',
                          style: TextStyle(
                              fontFamily: 'Caveat-Bold',
                              fontSize: ScreenUtil.getInstance().setSp(30)),
                        ),
                        TextField(
                          controller: usernameController,
                          decoration: InputDecoration(
                              hintText: 'enter username here',
                              hintStyle: TextStyle(
                                  color: Colors.grey,
                                  fontSize: 12.0,
                                  fontFamily: 'Caveat-Bold')),
                        ),
                        SizedBox(
                          height: ScreenUtil.getInstance().setHeight(20),
                        ),
                        Text(
                          'Password',
                          style: TextStyle(
                              fontFamily: 'Caveat-Bold',
                              fontSize: ScreenUtil.getInstance().setSp(30)),
                        ),
                        TextField(
                          controller: passwordController,
                          obscureText: true,
                          decoration: InputDecoration(
                              hintText: 'enter password here',
                              hintStyle: TextStyle(
                                  color: Colors.grey,
                                  fontSize: 12.0,
                                  fontFamily: 'Caveat-Bold')),
                        ),
                        SizedBox(
                          height: ScreenUtil.getInstance().setHeight(35),
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: <Widget>[
                            FlatButton(
                              onPressed: () {
                        
                                return showDialog(
                                    context: context,
                                    builder: (context) {
                                      return AlertDialog(
                                        actions: <Widget>[
                                          FlatButton(
                                            child: Text('Confirm'),
                                            onPressed: () {
                                              String email = emailController.text.trim();
                                              _auth.sendPasswordResetEmail(
                                                  email: email);
                                          

                                              showDialog(
                                                  context: context,
                                                  builder: (context) {
                                                    return AlertDialog(
                                                      content: Text(
                                                          'An email was sent to $email, go to your emails to reset your password.'),
                                                      title: Text('Email Sent'),
                                                      actions: <Widget>[
                                                        FlatButton(
                                                          onPressed: () {
                                                            Navigator.of(
                                                                    context)
                                                                .pop();
                                                          },
                                                          child: Text('Close'),
                                                        ),
                                                      ],
                                                    );
                                                  });
                                              //}
                                              //Navigator.of(context).pop();
                                            },
                                          ),
                                          FlatButton(
                                            child: Text('Close'),
                                            onPressed: () {
                                              Navigator.of(context).pop();
                                            },
                                          )
                                        ],
                                        content: TextField(
                                          controller: emailController,
                                          decoration: InputDecoration(
                                              hintText:
                                                  'Enter Your Email Address'),
                                        ),
                                      );
                                    });
                              },
                              child: Text(
                                'forgot password',
                                style: TextStyle(
                                    color: Colors.blue,
                                    fontFamily: 'Caveat-Bold',
                                    fontSize:
                                        ScreenUtil.getInstance().setSp(28)),
                              ),
                            )
                          ],
                        )
                      ],
                    ),
                  ),
                ),
                SizedBox(
                  height: ScreenUtil.getInstance().setHeight(30),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    FlatButton(
                      onPressed: _radio,
                      child: Row(
                        children: <Widget>[
                          SizedBox(
                            width: 12.0,
                          ),
                          GestureDetector(
                            onTap: _radio,
                            child: radioButton(_isSelected),
                          ),
                          SizedBox(
                            width: 8.0,
                          ),
                          Text(
                            'Remember Me',
                            style: TextStyle(
                                fontSize: 12.0, fontFamily: 'Caveat-Bold'),
                          )
                        ],
                      ),
                    ),
                    InkWell(
                      child: Container(
                        width: ScreenUtil.getInstance().setWidth(330),
                        height: ScreenUtil.getInstance().setHeight(100),
                        decoration: BoxDecoration(
                            gradient: LinearGradient(
                                colors: [Color(0xFF17ead9), Color(0xFF6078ea)]),
                            borderRadius: BorderRadius.circular(6.0),
                            boxShadow: [
                              BoxShadow(
                                  color: Color(0xFF6078ea).withOpacity(.3),
                                  offset: Offset(0.0, 8.0),
                                  blurRadius: 8.0)
                            ]),
                        child: Material(
                          color: Colors.transparent,
                          child: InkWell(
                            onTap: () async {
                              username = usernameController.text;
                              password = passwordController.text;
                              if (username == null ||
                                  username.isEmpty ||
                                  username == '') {
                                showDialog(
                                    context: context,
                                    builder: (context) {
                                      return AlertDialog(
                                        title: Text('Error'),
                                        actions: <Widget>[
                                          FlatButton(
                                            onPressed: () {
                                              Navigator.of(context).pop();
                                            },
                                            child: Text('close'),
                                          )
                                        ],
                                        content:
                                            Text('Please enter  your username'),
                                      );
                                    });
                              } else if (password == null ||
                                  password.isEmpty ||
                                  password == '') {
                                showDialog(
                                    context: context,
                                    builder: (context) {
                                      return AlertDialog(
                                        title: Text('Error'),
                                        actions: <Widget>[
                                          FlatButton(
                                            onPressed: () {
                                              Navigator.of(context).pop();
                                            },
                                            child: Text('close'),
                                          )
                                        ],
                                        content:
                                            Text('Please enter your password'),
                                      );
                                    });
                              } else {
                                try {
                                  _scaffoldKey.currentState
                                      .showSnackBar(new SnackBar(
                                          duration: Duration(seconds: 4),
                                          content: new Row(
                                            children: <Widget>[
                                              CircularProgressIndicator(),
                                              SizedBox(
                                                width: 10,
                                              ),
                                              Text('Signing In ...')
                                            ],
                                          )));

                                  usernameController.clear();
                                  passwordController.clear();

                                  await signIn(username.trim(), password);
                                } catch (e) {
                                  showDialog(
                                      context: context,
                                      builder: (context) {
                                        return AlertDialog(
                                          actions: <Widget>[
                                            FlatButton(
                                              onPressed: () {
                                                Navigator.of(context).pop();
                                              },
                                              child: Text('close'),
                                            )
                                          ],
                                          title: Text('Error'),
                                          content:
                                              Text('Invalid email or password'),
                                        );
                                      });
                                }
                              }
                            },
                            child: Center(
                              child: Text("SIGNIN",
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontFamily: "Caveat-Bold",
                                      fontSize: 18,
                                      letterSpacing: 1.0)),
                            ),
                          ),
                        ),
                      ),
                    )
                  ],
                ),
                SizedBox(
                  height: ScreenUtil.getInstance().setHeight(40),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    horizontalLine(),
                    FlatButton(
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => SignUp()),
                        );
                      },
                      child: Text("Dont Have an Account? TAP ME",
                          style: TextStyle(
                              fontSize: 14.0, fontFamily: "Caveat-Bold")),
                    ),
                    horizontalLine()
                  ],
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}

class ProgressBar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: ProgressBar(),
    );
  }
}
