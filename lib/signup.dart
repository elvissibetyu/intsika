import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:firebase_auth/firebase_auth.dart';

import 'package:intsika/main_menu.dart';

void main() => runApp(MaterialApp(
  home: SignUp(),
));

class SignUp extends StatefulWidget {

  @override
  _SignUpState createState() => _SignUpState();
}

class _SignUpState extends State<SignUp> {

  int dropdownValue = 12; // first value that initially appears on the dropdown.
  String fullName;
  int grade;
  String userName;
  String password;
  String confirmPassword;

  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  final FirebaseAuth _auth = FirebaseAuth.instance;

  Future<String> signUp(String email, String password) async{
    AuthResult user = await _auth.createUserWithEmailAndPassword(email: email, password: password);
    FirebaseAuth.instance.currentUser().then((val) {
      UserUpdateInfo updateUser = UserUpdateInfo();
      updateUser.displayName = fullName;
      //updateUser.photoUrl = picURL;
      val.updateProfile(updateUser);
    });
    Navigator.pushReplacement(context,
      MaterialPageRoute(builder: (context) => Menu()),
    );
  }


  // create a controller to retrieve the current value of the text field.
  final nameController = TextEditingController();
  final userNameController = TextEditingController();
  final passwordController = TextEditingController();
  final confirmPasswordController = TextEditingController();

  @override
  void dispose(){
    // cleans up the controller when the widget is disposed.
    nameController.dispose();
    userNameController.dispose();
    passwordController.dispose();
    confirmPasswordController.dispose();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        centerTitle: true,
        title: Text(
          'SIGNUP',
          style: TextStyle(
            fontFamily: 'Caveat-Bold',
            fontSize: ScreenUtil.getInstance().setSp(48),
            color: Colors.white
          ),
        ),
      ),
      body: SingleChildScrollView(
        child: Container(
          width: double.infinity,
          height: ScreenUtil.getInstance().setHeight(1200),
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(8.0),
              boxShadow: [
                BoxShadow(
                    color: Colors.black12,
                    offset: Offset(0.0, 15.0),
                    blurRadius: 15.0
                ),
                BoxShadow(
                    color: Colors.black12,
                    offset: Offset(0.0, -10.0),
                    blurRadius: 10.0
                )
              ]
          ),
          child: Padding(
            padding: EdgeInsets.only(left: 16.0, right: 16.0, top: 16.0),
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    'Student Information',
                    style: TextStyle(
                      fontFamily: "Caveat-Bold",
                      fontSize: ScreenUtil.getInstance().setSp(50),
                      letterSpacing: .6,
                    ),
                  ),
                  SizedBox(height: ScreenUtil.getInstance().setHeight(30),),
                  Text(
                    'Fullname',
                    style: TextStyle(
                        fontFamily: 'Caveat-Bold',
                        fontSize: ScreenUtil.getInstance().setSp(30)
                    ),
                  ),
                  TextField(
                    controller: nameController,
                    decoration: InputDecoration(
                        hintText: 'enter full name here',
                        hintStyle: TextStyle(
                            color: Colors.grey,
                            fontSize: 12.0,
                            fontFamily: 'Caveat-Bold'
                        )
                    ),
                  ),
                  SizedBox(height: ScreenUtil.getInstance().setHeight(20),),
                  Row(
                    children: <Widget>[
                      Text(
                        'Grade',
                        style: TextStyle(
                            fontFamily: 'Caveat-Bold',
                            fontSize: ScreenUtil.getInstance().setSp(30)
                        ),
                      ),
                      SizedBox(width: 15.0,),
                      DropdownButton<int>(
                        value: dropdownValue,
                        icon: Icon(
                            Icons.arrow_drop_down_circle
                        ),
                        elevation: 16,
                        style: TextStyle(
                            color: Colors.blue,
                            fontSize: 12,
                            fontFamily: 'Caveat-Bold'
                        ),
                        underline:  Container(
                          height: 2,
                          color: Colors.deepPurpleAccent,
                        ),
                        onChanged: (int data){
                          setState(() {
                            dropdownValue = data;
                          });
                        },
                        items: [10,11,12].map<DropdownMenuItem<int>>((int value){
                          return DropdownMenuItem<int>(
                            value: value,
                            child: Text('$value'),
                          );
                        }).toList(),
                      ),
                    ],
                  ),

                  SizedBox(height: ScreenUtil.getInstance().setHeight(20),),
                  Text(
                    'Username',
                    style: TextStyle(
                        fontFamily: 'Caveat-Bold',
                        fontSize: ScreenUtil.getInstance().setSp(30)
                    ),
                   ),
                  TextField(
                    controller: userNameController,
                    decoration: InputDecoration(
                        hintText: 'enter username here',
                        hintStyle: TextStyle(
                            color: Colors.grey,
                            fontSize: 12.0,
                            fontFamily: 'Caveat-Bold'
                        )
                    ),
                  ),
                  SizedBox(height: ScreenUtil.getInstance().setHeight(20),),
                  Text(
                    'Password',
                    style: TextStyle(
                        fontFamily: 'Caveat-Bold',
                        fontSize: ScreenUtil.getInstance().setSp(30)
                    ),
                  ),
                  TextField(
                    controller: passwordController,
                    obscureText: true,
                    decoration: InputDecoration(
                        hintText: 'enter password here',
                        hintStyle: TextStyle(
                            color: Colors.grey,
                            fontSize: 12.0,
                            fontFamily: 'Caveat-Bold'
                        )
                    ),
                  ),
                  SizedBox(height: ScreenUtil.getInstance().setHeight(30),),
                  Text(
                    'Confirm Password',
                    style: TextStyle(
                        fontFamily: 'Caveat-Bold',
                        fontSize: ScreenUtil.getInstance().setSp(30)
                    ),
                  ),
                  TextField(
                    controller: confirmPasswordController,
                    obscureText: true,
                    decoration: InputDecoration(
                        hintText: 'enter password here',
                        hintStyle: TextStyle(
                            color: Colors.grey,
                            fontSize: 12.0,
                            fontFamily: 'Caveat-Bold'
                        )
                    ),
                  ),
                  SizedBox(height: ScreenUtil.getInstance().setHeight(50),),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      InkWell(
                        child: Container(
                          width: ScreenUtil.getInstance().setWidth(330),
                          height: ScreenUtil.getInstance().setHeight(100),
                          decoration: BoxDecoration(
                              gradient: LinearGradient(colors: [
                                Color(0xFF17ead9),
                                Color(0xFF6078ea)
                              ]),
                              borderRadius: BorderRadius.circular(6.0),
                              boxShadow: [
                                BoxShadow(
                                    color: Color(0xFF6078ea).withOpacity(.3),
                                    offset: Offset(0.0, 8.0),
                                    blurRadius: 8.0)
                              ]),
                          child: Material(
                            color: Colors.transparent,
                            child: InkWell(
                              onTap: () async {
                                // gets the text in the text fields and store it the the variables.
                                fullName = nameController.text;
                                userName = userNameController.text;
                                password = passwordController.text;
                                confirmPassword = confirmPasswordController.text;
                                //nameController.dispose();
                                if(fullName == '' || userName == '' || password == '' || confirmPassword == ''){
                                  showDialog(
                                    context: context,
                                    builder: (context){
                                      return AlertDialog(
                                        content: Text('All fields must be filled.'),
                                        title: Text('Error'),
                                        actions: <Widget>[
                                          FlatButton(
                                            child: Text('close'),
                                            onPressed: (){
                                              Navigator.of(context).pop();
                                            },
                                          )
                                        ],
                                      );
                                    }
                                  );
                                }
                                else if(password != confirmPassword){
                                  passwordController.clear();
                                  confirmPasswordController.clear();
                                  showDialog(
                                    context: context,
                                    builder: (context){
                                      return AlertDialog(
                                        content: Text('Passwords do not match'),
                                        title: Text('Error'),
                                        actions: <Widget>[
                                          FlatButton(
                                            child: Text('close'),
                                            onPressed: (){
                                              Navigator.of(context).pop();
                                            },
                                          )
                                        ],
                                      );
                                    }
                                  );
                                }
                                else if(password.length < 6){
                                  passwordController.clear();
                                  confirmPasswordController.clear();
                                  showDialog(
                                    context: context,
                                    builder: (context){
                                      return AlertDialog(
                                        content: Text('Password too short, must be 6 or more characters'),
                                        title: Text('Error'),
                                        actions: <Widget>[
                                          FlatButton(
                                            child: Text('close'),
                                            onPressed: (){
                                              Navigator.of(context).pop();
                                            },
                                          )
                                        ],
                                      );
                                    }
                                  );
                                }
                                else{
                                  // clears the text fields after getting the values
                                  try {
                                    nameController.clear();
                                    userNameController.clear();
                                    passwordController.clear();
                                    confirmPasswordController.clear();

                                    _scaffoldKey.currentState.showSnackBar(
                                        new SnackBar(duration: Duration(seconds: 4),content: new Row(
                                          children: <Widget>[
                                            CircularProgressIndicator(),
                                            SizedBox(width: 10,),
                                            Text('Creating Account ...')
                                          ],
                                        ))
                                    );

                                    await signUp(userName.trim(), password);
                                  }
                                  catch(e){
                                    showDialog(
                                        context: context,
                                      builder: (context){
                                        return AlertDialog(
                                          content: Text('Please check your email and retry'),
                                          title: Text('Error'),
                                          actions: <Widget>[
                                            FlatButton(
                                              child: Text('close'),
                                              onPressed: (){
                                                Navigator.of(context).pop();
                                              },
                                            )
                                          ],
                                        ) ;
                                      }
                                    );
                                  }
                                }
                              },
                              child: Center(
                                child: Text("SIGNUP",
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontFamily: "Caveat-Bold",
                                        fontSize: 18,
                                        letterSpacing: 1.0)),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: ScreenUtil.getInstance().setHeight(50),),
                  Text(
                    'By Creating this account you taking a huge step towards the right direction with your academics.',
                    style: TextStyle(
                      fontFamily: 'Caveat-Bold',
                      fontSize: ScreenUtil.getInstance().setSp(30),
                      color: Colors.blue
                    )
                  ),
                  SizedBox(height: ScreenUtil.getInstance().setHeight(35),),
                  Text(
                      'CLAP FOR YOURSELF.',
                      style: TextStyle(
                          fontFamily: 'Caveat-Bold',
                          fontSize: ScreenUtil.getInstance().setSp(30),
                          color: Colors.blue
                      )
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
