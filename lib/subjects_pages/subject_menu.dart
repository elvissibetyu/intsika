import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:intsika/subjects_pages/m_choice_landing.dart';
import 'package:intsika/subjects_pages/picture_quiz.dart';
import 'package:intsika/subjects_pages/quiz_landing.dart';
import 'package:path_provider/path_provider.dart';
import 'package:intsika/e_textbooks/textbooks.dart';

class SubjectMenu extends StatefulWidget {
  final String subject;
  final List<String> items;
  final List<Color> colors;

  const SubjectMenu({Key key, this.subject, this.items, this.colors}) : super(key: key);

  @override
  _SubjectMenuState createState() => _SubjectMenuState();
}

class _SubjectMenuState extends State<SubjectMenu> {
  Future<File> getFileFromAsset(String asset, String fileName) async {
    try {
      var data = await rootBundle.load(asset);
      var bytes = data.buffer.asUint8List();
      var dir = await getApplicationDocumentsDirectory();
      File file = File('${dir.path}/$fileName');
      File assetFile = await file.writeAsBytes(bytes);
      return assetFile;
    } catch (e) {
      throw Exception('Could not load url file' + e.toString());
    }
  }

  String mathPath;
  String physicsPath;
  String lifeSciencePath;
  String mathLitPath;
  String economicsPath;
  String lifePath;
  String sippPath;
  String viaPath;
  String accounting;
  String accounting2;
@override
  void initState() {
    getFileFromAsset('assets/MathematicsResource2019.pdf', 'MathsTelematics').then((f) {
                      setState(() {
                       mathPath = f.path;
                      });
                  });
    getFileFromAsset('assets/PhysicalSciencesResource2019.pdf', 'PhysicsTelematics').then((f) {
                      setState(() {
                       physicsPath = f.path;
                      });
                  });
    getFileFromAsset('assets/LifeSciencesResource2019.pdf', 'lifeScienceTelematics').then((f) {
                      setState(() {
                       lifeSciencePath = f.path;
                      });
                  });
    getFileFromAsset('assets/MTG Maths_Lit_Gr12_web_compressed.pdf', 'MTGMathsLit').then((f) {
                      setState(() {
                       mathLitPath = f.path;
                      });
                  });
    getFileFromAsset('assets/2b MTG Econ EN 18 Sept 2014_compressed (1).pdf', 'MTGecossLit').then((f) {
                      setState(() {
                       economicsPath = f.path;
                      });
                  });
    getFileFromAsset('assets/4b MTG LifeSci EN 18 Sept 2014_compressed.pdf', 'MTGlifeScience').then((f) {
                      setState(() {
                       lifePath = f.path;
                      });
                  });
    getFileFromAsset('assets/mathslitgr12ssipbookletsessions1-2ln2013_compressed.pdf', 'sippMathsLit').then((f) {
                      setState(() {
                       sippPath = f.path;
                      });
                  });
    getFileFromAsset('assets/viaAfrikaMathsLitStudy_compressed (1).pdf', 'viaAfrikaMaths').then((f) {
                      setState(() {
                       viaPath = f.path;
                      });
                  });
    getFileFromAsset('assets/1b MTG Accounting EN 18 Sept 2014_compressed (1).pdf', 'MTGAcc').then((f) {
                      setState(() {
                       accounting = f.path;
                      });
                  });
    getFileFromAsset('assets/AccountingResource2019.pdf', 'TeleAcc').then((f) {
                      setState(() {
                       accounting2= f.path;
                      });
                  });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          '${widget.subject}',
          style: TextStyle(
            fontFamily: 'Caveat-Bold',
            fontSize: ScreenUtil.getInstance().setSp(50),
          ),
        ),
      ),
      body: ListView.builder(
          padding: EdgeInsets.fromLTRB(5, 15, 5, 5),
          itemCount: widget.items.length,
          itemBuilder: (context, index) {
            return Card(
              color: widget.colors[index],
              elevation: 2,
              borderOnForeground: true,
              child: ListTile(
                onTap: (){
                  if(widget.items[index] == 'Maths QUIZ'){
                    Navigator.push(context, MaterialPageRoute(builder: (context) => QuizLanding(subject: 'Maths',)));
                  }
                  else if(widget.items[index] == 'Maths Telematics'){
                    Navigator.push(context, MaterialPageRoute(builder: (context) => PDFViewPage(path: mathPath,)));
                  }
                  else if(widget.items[index] == 'Physical Sciences Telematics'){
                    Navigator.push(context, MaterialPageRoute(builder: (context) => PDFViewPage(path: physicsPath,)));
                  }
                  else if(widget.items[index] == 'Life Sciences Telematics'){
                    Navigator.push(context, MaterialPageRoute(builder: (context) => PDFViewPage(path: lifeSciencePath,)));
                  }
                  else if(widget.items[index] == 'Mind The Gap - Maths Literacy'){
                    Navigator.push(context, MaterialPageRoute(builder: (context) => PDFViewPage(path: mathLitPath,)));
                  }
                  else if(widget.items[index] == 'Mind The Gap - Economics'){
                    Navigator.push(context, MaterialPageRoute(builder: (context) => PDFViewPage(path: economicsPath,)));
                  }
                  else if(widget.items[index] == 'Mind The Gap - Life Sciences'){
                    Navigator.push(context, MaterialPageRoute(builder: (context) => PDFViewPage(path: lifePath,)));
                  }
                  else if(widget.items[index] == 'SIPP Learners Notes - Maths Lit'){
                    Navigator.push(context, MaterialPageRoute(builder: (context) => PDFViewPage(path: sippPath,)));
                  }
                  else if(widget.items[index] == 'Via Afrika Study Guide - Maths Lit'){
                    Navigator.push(context, MaterialPageRoute(builder: (context) => PDFViewPage(path: viaPath,)));
                  }
                  else if(widget.items[index] == 'Mind The Gap - Accounting'){
                    Navigator.push(context, MaterialPageRoute(builder: (context) => PDFViewPage(path: accounting,)));
                  }
                  else if(widget.items[index] == 'Telematics - Accouting'){
                    Navigator.push(context, MaterialPageRoute(builder: (context) => PDFViewPage(path: accounting2,)));
                  }
                  else if(widget.items[index] == 'Maths Lit QUIZ'){
                    Navigator.push(context, MaterialPageRoute(builder: (context) => QuizLanding(subject: 'Maths Lit',)));
                  }
                  else if(widget.items[index] == 'Life Sciences QUIZ1'){
                    Navigator.push(context, MaterialPageRoute(builder: (context) => QuizLanding(subject: 'Life Sciences1',)));
                  }
                  else if(widget.items[index] == 'Life Sciences QUIZ2'){
                    Navigator.push(context, MaterialPageRoute(builder: (context) => QuizLanding(subject: 'Life Sciences2',)));
                  }
                  else if(widget.items[index] == 'Physics QUIZ1'){
                    Navigator.push(context, MaterialPageRoute(builder: (context) => QuizLanding(subject: 'Physics1',)));
                  }
                  else if(widget.items[index] == 'Physics QUIZ2'){
                    Navigator.push(context, MaterialPageRoute(builder: (context) => QuizLanding(subject: 'Physics2',)));
                  }
                  else if(widget.items[index] == 'Chemistry QUIZ1'){
                    Navigator.push(context, MaterialPageRoute(builder: (context) => QuizLanding(subject: 'Chemistry1',)));
                  }
                  else if(widget.items[index] == 'Chemistry QUIZ2'){
                    Navigator.push(context, MaterialPageRoute(builder: (context) => QuizLanding(subject: 'Chemistry2',)));
                  }
                  else if(widget.items[index] == 'Economics QUIZ1'){
                    Navigator.push(context, MaterialPageRoute(builder: (context) => QuizLanding(subject: 'Economics1',)));
                  }
                  else if(widget.items[index] == 'Economics QUIZ2'){
                    Navigator.push(context, MaterialPageRoute(builder: (context) => QuizLanding(subject: 'Economics2',)));
                  }
                  else if(widget.items[index] == 'Multiple choice Questions' && widget.subject == 'Mathematics'){
                    Navigator.push(context, MaterialPageRoute(builder: (context) => MultipleChoiceLanding(subject: 'Maths',)));
                  }
                  else if(widget.items[index] == 'Multiple choice Questions2' && widget.subject == 'Mathematics'){
                    Navigator.push(context, MaterialPageRoute(builder: (context) => MultipleChoiceLanding(subject: 'Maths2',)));
                  }
                  else if(widget.items[index] == 'Multiple choice Questions Physics' && widget.subject == 'Physical Sciences'){
                    Navigator.push(context, MaterialPageRoute(builder: (context) => MultipleChoiceLanding(subject: 'Physical Sciences - Phy',)));
                  }
                  else if(widget.items[index] == 'Multiple choice Questions Chemistry' && widget.subject == 'Physical Sciences'){
                    Navigator.push(context, MaterialPageRoute(builder: (context) => MultipleChoiceLanding(subject: 'Physical Sciences - Chem',)));
                  }
                  else if(widget.items[index] == 'Multiple choice Questions' && widget.subject == 'Life Sciences'){
                    Navigator.push(context, MaterialPageRoute(builder: (context) => MultipleChoiceLanding(subject: 'Life Sciences',)));
                  }
                  else if(widget.items[index] == 'Multiple choice Questions' && widget.subject == 'Economics'){
                    Navigator.push(context, MaterialPageRoute(builder: (context) => MultipleChoiceLanding(subject: 'Economics',)));
                  }
                  else if(widget.items[index] == 'Multiple choice Questions' && widget.subject == 'Mathematics Literacy'){
                    Navigator.push(context, MaterialPageRoute(builder: (context) => PictureQuiz()));
                  }
                },
                dense: true,
                title: Text('${widget.items[index]}', 
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  color: Colors.white,
                  fontSize: ScreenUtil.getInstance().setSp(35)
                ),),
                leading: Icon(Icons.brightness_1, color: Colors.blue,),
                trailing: Icon(Icons.keyboard_arrow_right, color: Colors.white,),
              ),
            );
          }),
    );
  }
}
