import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:intsika/subjects_pages/subject_menu.dart';

void main() => runApp(new MaterialApp(
  theme: new ThemeData(primaryColor: Color.fromRGBO(6, 26, 46, 1.0)),
  home: new ListPage(),
));

class ListPage extends StatefulWidget {
  ListPage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _ListPageState createState() => _ListPageState();
}

class _ListPageState extends State<ListPage> {
  final topAppBar = AppBar(
    elevation: 15.0,

    //backgroundColor: Colors.white,
    title: Text(
            'Subjects',
            style: TextStyle(
            fontFamily: 'Caveat-Bold',
              fontSize: ScreenUtil.getInstance().setSp(50),
              color: Colors.white
        ),
    ),
    centerTitle: false,
  );

  final makeBody = Container(
    padding: EdgeInsets.only(top: ScreenUtil.getInstance().setHeight(30)),
    child: ListView(
        children: <Widget>[
          MakeListTile(text: 'Mathematics', color: Colors.red[400]),
          MakeListTile(text: 'Mathematics Literacy', color: Colors.blueAccent),
          MakeListTile(text: 'Physical Sciences', color: Colors.orange[200],),
          MakeListTile(text: 'Life Sciences', color: Colors.green[600],),
          MakeListTile(text: 'Economics', color: Colors.blue,),
          MakeListTile(text: 'Accounting', color: Colors.pink[900],)
        ],
    ),
  );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: topAppBar,
      body: makeBody,
    );
  }
}

class MakeListTile extends StatelessWidget {

  final String text;
  final Color color;
  getTapped(){
    return text;
  }

  MakeListTile({this.text, this.color});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: (){
        String tapped = getTapped();
        if(tapped == 'Mathematics'){
          List<String> textbooks = ['Maths Telematics','Multiple choice Questions','Multiple choice Questions2', 'Maths QUIZ'];
          List<Color> colors = [Colors.red, Colors.green[600], Colors.pink[900], Colors.blue[700]];
          Navigator.push(context, MaterialPageRoute(builder: (context) => SubjectMenu(items:  textbooks, subject: tapped, colors: colors,)));
        }
        else if(tapped == 'Mathematics Literacy'){
          List<String> textbooks = ['Mind The Gap - Maths Literacy','SIPP Learners Notes - Maths Lit', 'Via Afrika Study Guide - Maths Lit'];
          List<Color> colors = [Colors.red, Colors.green[600], Colors.blue[700]];
          Navigator.push(context, MaterialPageRoute(builder: (context) => SubjectMenu(items:  textbooks, subject: tapped, colors: colors,)));
        }
        else if(tapped == 'Life Sciences'){
          List<String> textbooks = ['Mind The Gap - Life Sciences','Life Sciences Telematics', 'Multiple choice Questions', 'Life Sciences QUIZ1', 'Life Sciences QUIZ2'];
          List<Color> colors = [Colors.red, Colors.green[600], Colors.blue[700], Colors.orange[800], Colors.pink[900]];
          Navigator.push(context, MaterialPageRoute(builder: (context) => SubjectMenu(items:  textbooks, subject: tapped, colors: colors,)));
        }
        else if(tapped == 'Physical Sciences'){
          List<String> textbooks = ['Physical Sciences Telematics', 'Mind The Gap - Chemistry', 'Multiple choice Questions Physics', 'Multiple choice Questions Chemistry', 'Physics QUIZ1', 'Physics QUIZ2', 'Chemistry QUIZ1', 'Chemistry QUIZ2'];
          List<Color> colors = [Colors.red, Colors.blue[700], Colors.blueGrey[700], Colors.orange[600], Colors.green[600], Colors.blueAccent, Colors.pink[900], Colors.red[800]];
          Navigator.push(context, MaterialPageRoute(builder: (context) => SubjectMenu(items:  textbooks, subject: tapped, colors: colors,)));
        }
        else if(tapped == 'Economics'){
          List<String> textbooks = ['Mind The Gap - Economics', 'Multiple choice Questions', 'Economics QUIZ1', 'Economics QUIZ2' ];
          List<Color> colors = [Colors.red, Colors.green[600], Colors.blue[700], Colors.pink[900]];
          Navigator.push(context, MaterialPageRoute(builder: (context) => SubjectMenu(items:  textbooks, subject: tapped, colors: colors,)));
        }
        else if(tapped == 'Accounting'){
          List<String> textbooks = ['Mind The Gap - Accounting', 'Telematics - Accouting',];
          List<Color> colors = [Colors.red, Colors.green[600],];
          Navigator.push(context, MaterialPageRoute(builder: (context) => SubjectMenu(items:  textbooks, subject: tapped, colors: colors,)));
        }


      },
      child: Card(
        elevation: 8.0,
        margin: new EdgeInsets.symmetric(horizontal: 10.0, vertical: 6.0),
        child: Container(
          decoration: BoxDecoration(color: color),
          child: ListTile(
              contentPadding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 10.0),
              leading: Container(
                padding: EdgeInsets.only(right: 12.0),
                decoration: new BoxDecoration(
                    border: new Border(
                        right: new BorderSide(width: 1.0, color: Colors.white24))),
                child: Icon(Icons.library_books, color: Colors.white),
              ),
              title: Text(
                text,
                style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
              ),
// subtitle: Text("Intermediate", style: TextStyle(color: Colors.white)),
              subtitle: Row(
                children: <Widget>[
                  Icon(Icons.school, color: Colors.yellowAccent),
                  Text(" Grade 12", style: TextStyle(color: Colors.white))
                ],
              ),
              trailing:
              Icon(Icons.keyboard_arrow_right, color: Colors.white, size: 30.0)),
        ),
      ),
    );
  }
}

