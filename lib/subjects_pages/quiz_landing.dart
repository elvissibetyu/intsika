import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:intsika/subjects_pages/question.dart';
import 'package:intsika/subjects_pages/quiz.dart';
import 'package:intsika/subjects_pages/quiz_page.dart';

void main() => runApp(MaterialApp(
      home: QuizLanding(),
    ));

class QuizLanding extends StatelessWidget {
  final String subject;
  const QuizLanding({Key key, this.subject}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.blueAccent,
      child: InkWell(
        splashColor: Colors.blue[900],
        onTap: () {
          if (subject == 'Maths') {
            Quiz quiz = Quiz([
              Question('Trigonometry is nice', false),
              Question('Maths is lit', false),
              Question('Fuck Maths ne?', true)
            ]);
            Navigator.pushReplacement(
                context,
                MaterialPageRoute(
                    builder: (context) => QuizPage(
                          quiz: quiz,
                          subject: subject,
                        )));
          } else if (subject == 'Physics1') {
            Quiz quiz = Quiz([
              Question(
                  "The net (resultant) force acting on an object is equal to the rate of change of momentum of the object in the direction of the net force.",
                  true),
              Question(
                  "inertia is a physical quantity that is described as a measure of the resistance of a body to a change in motion is called .",
                  true),
              Question(
                  "When an object falls freely in a vacuum near the surface of the Earth, the rate of change of velocity of the object will increase uniformly.",
                  false),
              Question(
                  "Electrical to mechanical takes place when an AC generator is in operation",
                  false),
              Question(
                  "Frictional force always acts perpendicular to the surface on which a body is placed",
                  false),
              Question(
                  "The speed of a bicycle increases from 2 m∙s-1 to 8 m∙s-1. Its kinetic energy increases by a factor of 16 ",
                  true),
              Question(
                  "The spectrum of an element from a star shows some absorption lines. These lines are produced because...a cold gas absorbs certain frequencies of light passing through it.",
                  true),
              Question(
                  "Light reaching the Earth from a galaxy moving away is shifted towards higher frequencies.",
                  false),
              Question(
                  "The minimum value of the resistance that can be obtained by connecting two 4 Ω resistors is ...8 Ω",
                  false),
              Question(
                  "The tendency of an object to remain at rest or to continue in its uniform motion in a straight line is known as Newton's Third Law.",
                  false)
            ]);
            Navigator.pushReplacement(
                context,
                MaterialPageRoute(
                    builder: (context) => QuizPage(
                          quiz: quiz,
                          subject: subject,
                        )));
          } else if (subject == 'Physics2') {
            Quiz quiz = Quiz([
              Question(
                  "Power is defined as the rate of change of momentum.", false),
              Question("Electrostatic force is an example of a contact force.",
                  false),
              Question(
                  "destructive interference is defined as two light sources of the same frequency maintain the same phase relationship with each other.",
                  false),
              Question(
                  "Electrical to mechanical takes place when an AC generator is in operation",
                  false),
              Question(
                  "Frictional force always acts perpendicular to the surface on which a body is placed",
                  false),
              Question(
                  "The speed of a bicycle increases from 2 m∙s-1 to 8 m∙s-1. Its kinetic energy increases by a factor of 16 ",
                  true),
              Question(
                  "The spectrum of an element from a star shows some absorption lines. These lines are produced because...a cold gas absorbs certain frequencies of light passing through it.",
                  true),
              Question(
                  "Light reaching the Earth from a galaxy moving away is shifted towards higher frequencies.",
                  false),
              Question(
                  "The minimum value of the resistance that can be obtained by connecting two 4 Ω resistors is ...8 Ω",
                  false),
              Question(
                  "The tendency of an object to remain at rest or to continue in its uniform motion in a straight line is known as Newton's Third Law.",
                  false)
            ]);
            Navigator.pushReplacement(
                context,
                MaterialPageRoute(
                    builder: (context) => QuizPage(
                          quiz: quiz,
                          subject: subject,
                        )));
          } else if (subject == 'Chemistry1') {
            Quiz quiz = Quiz([
              Question(
                  "The minimum energy needed for a chemical reaction to occur is known as activation energy",
                  true),
              Question(
                  "A catalyst is a substance that decreases the rate of a reaction without being consumed in the reaction",
                  false),
              Question("A structural isomer of butane is 2,2-dimethylpropane.",
                  false),
              Question(
                  "In a galvanic (voltaic) cell, electrons move from the  anode to the cathode in the external circuit.",
                  false),
              Question("Potassiumis a primary nutrient needed by plants", true),
              Question(
                  "Alkenes have one triple bond between two carbon atoms. ",
                  false),
              Question(
                  "In a chemical reaction an oxidising agent will gain protons..",
                  false),
              Question(
                  "Eutrophication is a process whereby excess plant nutrients stimulate excessive growth of algae.",
                  true),
              Question(
                  "Water undergoes auto-ionisation. During this process a proton is transferred from one water molecule to another",
                  false),
              Question(
                  "Sulphuric acid is produced in industry by the Ostwald process.",
                  false)
            ]);
            Navigator.pushReplacement(
                context,
                MaterialPageRoute(
                    builder: (context) => QuizPage(
                          quiz: quiz,
                          subject: subject,
                        )));
          } else if (subject == 'Chemistry2') {
            Quiz quiz = Quiz([
              Question(
                  "Ammonia is the product formed in the Haber process", true),
              Question("A carbonyl group is the functional group of alcohols.",
                  false),
              Question(
                  "A compound with the general formula CnH2n+2 is an alkane.",
                  true),
              Question(
                  "The presence of dissolved fertilisers which are rich in nitrates and phosphate can lead to eutrophication  .",
                  true),
              Question(
                  "The reaction of propane with bromine can be classified as an elimination reaction",
                  false),
              Question("Mg vwill reduce Aℓ^3+ to Aℓ ", true),
              Question(
                  "Ostwald is the name of the industrial process used in the preparation of nitric acid.",
                  true),
              Question(
                  "The coating of one metal with another by the process of electrolysis is known as Electroplating .",
                  true),
              Question(
                  "The temperature of a substance is a measure of the average potential energy  of the particles",
                  false),
              Question(
                  "In a chemical reaction, the difference between the potential energy of the products and the potential energy of the reactants is equal to the rate of the reaction.",
                  false)
            ]);
            Navigator.pushReplacement(
                context,
                MaterialPageRoute(
                    builder: (context) => QuizPage(
                          quiz: quiz,
                          subject: subject,
                        )));
          } else if (subject == 'Life Sciences1') {
            Quiz quiz = Quiz([
              Question(
                  "An inherited disease that affects haemoglobin and changes the shape of red blood corpuscles is Sickle-cell anaemia",
                  true),
              Question("Foetus is attached to the mother's uterus", true),
              Question(
                  "Phenotype is the genetic make-up of an organism in respect of the alleles it possesses",
                  false),
              Question(
                  "The complete disappearance of a species from Earth is Extinction",
                  true),
              Question(
                  "The full complement of genes present in an organism is genome",
                  true),
              Question(
                  "The chromosome complement/condition of a body cell which contains two sets of chromosomes is called haploid",
                  false),
              Question(
                  "The point at which chromatids of homologous chromosomes cross over during meiosis is called genetic locus ",
                  false),
              Question(
                  "Central nervous system and the periheral nervous system, CNS & PNS are the main divisions of the human nervous system",
                  true),
              Question(
                  "The 3 types of neurons are Sensory (Afferent) Neurons, Glial Neurons, Parasympathetic Neurons",
                  false),
              Question(
                  "The spaces between Schwann Cells are called Nodes of Ranvier",
                  true)
            ]);
            Navigator.pushReplacement(
                context,
                MaterialPageRoute(
                    builder: (context) => QuizPage(
                          quiz: quiz,
                          subject: subject,
                        )));
          } else if (subject == 'Life Sciences2') {
            Quiz quiz = Quiz([Question("Describe the reflex arc as Receptors On Skin > Sensory Neurons > Interneurons > Motor Neurons > Effectors > Reaction",true),
		Question("Grey matter is found in the brain and spinal cord",false),
		Question("The variety of plant and animal species on earth is  ecosystem.",false),
		Question("A hormone that stimulates the maturation of sperm and puberty in males is called Testosterone.",true),
		Question("The tube in the male reproductive system that connects the epididymis with the urethra is sperm duct.",true),
		Question("The hormone that stimulates the mammary glands to secrete milk is called aldosterone.",false),
		Question("Cytokinesis is the division of the cytoplasm through the constriction of the cell membrane at the end of cell division.",true),
		Question("sperm cells in humans are temporarily stored in Prostate gland.",false),
		Question("Adrenalin prepares the human body for an emergency situation.",true),
		Question("amniotic fluid act as a shock absorber to protect the foetus.",true)]);
            Navigator.pushReplacement(
                context,
                MaterialPageRoute(
                    builder: (context) => QuizPage(
                          quiz: quiz,
                          subject: subject,
                        )));
          }
          else if(subject == 'Economics1'){
            Quiz quiz = Quiz([Question("Taxes and subsidies on production are taken into account when calculating GDP at basic prices..",true),
		Question("Factors that originate from outside the domestic economic system are referred to as indigenous.",false),
		Question("This is NOT one of the problems of the public sector provisioning: Nationalisation.",true),
		Question("Deliberate action by the South African Reserve Bank to lower the value of the rand is known as devaluation.",true),
		Question("The establishment of a firm by one country in another country is called foreign trade " ,false),
		Question("Economic development is the same as economic growth. ",false),
		Question("The indicator used to measure the performance of the economy is called an economic indicator.",true),
		Question("The flow of income and expenditure in a circular flow is called money flow.",true),
		Question("The application of a trade policy whereby the state discourages the importing of certain goods is Protectionism",true),
		Question("Aimed at increasing the economic livelihood of specific areas is Regional development/Spatial Development Initiative(SDI).",true)]);
            Navigator.pushReplacement(
                context,
                MaterialPageRoute(
                    builder: (context) => QuizPage(
                          quiz: quiz,
                          subject: subject,
                        )));
          }
          else if(subject == 'Economics2'){
            Quiz quiz = Quiz([Question("Taxes that relate to individual goods, are called taxes on income.",false),
		Question("Trends are measured through economic indicators in the economy.",true),
		Question("Putting laws in place to regulate activities is known as nationalisation",false),
		Question("A transaction relating to investment in businesses is called direct investment.",true),
		Question("Regional cooperation and integration among African states is provided by Nepad.", true),
		Question("Cash incentives to assist foreign investors who want to invest in new manufacturing businesses in South Africa is offered by the Foreign Investment Incentive.",true),
		Question("The initiative to enhance economic growth and create employment is known as Black Economic Empowerment.",false),
		Question("According to the UN classification, South Africa falls in the lower human development group of countries.",false),
		Question("A record of export and import merchandise is known as Trade balance",true),
		Question("Indicators concerned with inflation rates and the supply of money are Economic indicators.",true)]);


            Navigator.pushReplacement(
                context,
                MaterialPageRoute(
                    builder: (context) => QuizPage(
                          quiz: quiz,
                          subject: subject,
                        )));
          }
        },
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              '$subject Quizzz!',
              style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                  fontSize: ScreenUtil.getInstance().setSp(60)),
            ),
            Text(
              'Tap screen to start',
              style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                  fontSize: ScreenUtil.getInstance().setSp(35)),
            ),
            SizedBox(
              height: ScreenUtil.getInstance().setHeight(15),
            ),
            FlatButton(
              onPressed: () {
                Navigator.of(context).pop();
              },
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Icon(
                    Icons.arrow_back,
                    color: Colors.white,
                  ),
                  //  SizedBox(width: ScreenUtil.getInstance().setHeight(2),),
                  Text(
                    'Go back',
                    style: TextStyle(
                        color: Colors.white, fontWeight: FontWeight.bold),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
