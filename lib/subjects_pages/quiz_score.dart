import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class ScorePage extends StatelessWidget {

  final int score;
  final int numberOfQuestions;

  ScorePage(this.score, this.numberOfQuestions);

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.blueAccent,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text('Your Score is', style: TextStyle(color: Colors.white, fontSize: ScreenUtil.getInstance().setSp(80), fontWeight: FontWeight.bold),),
          Text('$score/$numberOfQuestions', style: TextStyle(color: Colors.white, fontSize: ScreenUtil.getInstance().setSp(80), fontWeight: FontWeight.bold),),

          FlatButton.icon(onPressed: (){
           Navigator.of(context).pop();
          }, icon: Icon(Icons.arrow_back, color: Colors.white,), label: Text('Exit Quiz'),)
        ],
      ),
    );
  }
}
