import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class PictureQuiz extends StatefulWidget {
  @override
  _PictureQuizState createState() => _PictureQuizState();
}

class _PictureQuizState extends State<PictureQuiz> {
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => false,
      child: Scaffold(
        body: Container(
          margin: EdgeInsets.all(ScreenUtil.getInstance().setHeight(17)),
          alignment: Alignment.topCenter,
          child: Column(
            children: <Widget>[
              Padding(padding: EdgeInsets.all(ScreenUtil.getInstance().setHeight(35)),),
              Text('Yess Kwedini')
            ],
          ),
        ),
      ),
    );
  }
}
