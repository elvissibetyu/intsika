import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class ResultPage extends StatefulWidget {

  final int marks;
  ResultPage({Key key, @required this.marks}) : super(key:key);

  @override
  _ResultPageState createState() => _ResultPageState(marks);
}

class _ResultPageState extends State<ResultPage> {

  int marks;
  _ResultPageState(this.marks);

  @override
  Widget build(BuildContext context) {

      return Material(
        color: Colors.blueAccent,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text('Your Score is:', style: TextStyle(fontWeight: FontWeight.bold, fontSize: ScreenUtil.getInstance().setSp(65), color: Colors.white),),
            SizedBox(height: ScreenUtil.getInstance().setHeight(20),),
            Text('$marks/50', style: TextStyle(fontSize: ScreenUtil.getInstance().setSp(65), color: Colors.white, fontWeight: FontWeight.bold),),
            FlatButton.icon(onPressed: (){Navigator.of(context).pop();}, icon: Icon(Icons.arrow_back, color: Colors.white, size: ScreenUtil.getInstance().setHeight(60),), label: Text('Exit Multiple Choice', style: TextStyle(color: Colors.white),))
          ],
        ),
    );
  }
}
