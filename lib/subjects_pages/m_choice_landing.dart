import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:intsika/subjects_pages/m_choice_page.dart';

class MultipleChoiceLanding extends StatelessWidget{

  final String subject;
  MultipleChoiceLanding({Key key, @required this.subject}) : super(key:key);

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.blueAccent,
      child: InkWell(
        onTap: (){
          if(subject == 'Maths'){
            Navigator.push(context, MaterialPageRoute(builder: (context) => GetJson(jsonFile: 'assets/maths.json',)));
          }
          if(subject == 'Maths2'){
            Navigator.push(context, MaterialPageRoute(builder: (context) => GetJson(jsonFile: 'assets/maths2.json',)));
          }
          else if(subject == 'Physical Sciences - Phy'){
            Navigator.push(context, MaterialPageRoute(builder: (context) => GetJson(jsonFile: 'assets/physics.json',)));
          }
          else if(subject == 'Physical Sciences - Chem'){
            Navigator.push(context, MaterialPageRoute(builder: (context) => GetJson(jsonFile: 'assets/Chemistry.json',)));
          }
          else if(subject == 'Life Sciences'){
            Navigator.push(context, MaterialPageRoute(builder: (context) => GetJson(jsonFile: 'assets/lifescience.json',)));
          }
          else if(subject == 'Economics'){
            Navigator.push(context, MaterialPageRoute(builder: (context) => GetJson(jsonFile: 'assets/Economics.json',)));
          }
        },
        splashColor: Colors.blue[900],
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(subject, style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: ScreenUtil.getInstance().setSp(60)),),
            Text('Multiple Choice Questions', style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: ScreenUtil.getInstance().setSp(50))),
            SizedBox(height: ScreenUtil.getInstance().setHeight(15),),
            Text('(Click anywhere on Screen to Proceed)', style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: ScreenUtil.getInstance().setSp(20))),
            SizedBox(height: ScreenUtil.getInstance().setHeight(15),),
            FlatButton.icon(onPressed: (){Navigator.of(context).pop();}, icon: Icon(Icons.arrow_back, color: Colors.white, size: ScreenUtil.getInstance().setHeight(45),), label: Text('Go back', style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: ScreenUtil.getInstance().setSp(40))))
          ],
        ),
      ),
    );
  }
}