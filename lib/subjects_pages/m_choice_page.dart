import 'dart:async';
import 'dart:convert';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter/material.dart';
import 'package:intsika/subjects_pages/m_choice_results.dart';

class GetJson extends StatelessWidget {
  final String jsonFile;
  GetJson({Key key, @required this.jsonFile}): super(key:key);
  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: DefaultAssetBundle.of(context).loadString(jsonFile),
      builder: (context, snapshot) {
        List myData = json.decode(snapshot.data.toString());
        if (myData == null) {
          return Scaffold(
            body: Center(
              child: Text('Loading...'),
            ),
          );
        } else {
          return QuizPage(
            myData: myData,
          );
        }
      },
    );
  }
}

class QuizPage extends StatefulWidget {
  var myData;

  QuizPage({Key key, @required this.myData}) : super(key: key);

  @override
  _QuizPageState createState() => _QuizPageState(myData);
}

class _QuizPageState extends State<QuizPage> {
  var myData;
  _QuizPageState(this.myData);

  Color colorToShow = Colors.indigoAccent;
  Color right = Colors.green;
  Color wrong = Colors.red;
  int marks = 0;
  int i = 1;
  int timer = 30;
  String showTimer = '30';

  Map<String, Color> btnColor = {
    "a" : Colors.indigoAccent,
    "b" : Colors.indigoAccent,
    "c" : Colors.indigoAccent,
    "d" : Colors.indigoAccent,
  };

  bool cancelTimer = false;

  @override
  void initState() {
    startTimer();
    super.initState();
  }

  void startTimer(){
    const oneSec = Duration(seconds: 1);
    Timer.periodic(oneSec, (Timer t){
      if(this.mounted){
        setState(() {
          if(timer < 1){
            t.cancel();
            nextQuestion();
          }else if(cancelTimer == true){
            t.cancel();
          }
          else{
            timer = timer - 1;
          }
          showTimer = timer.toString();
        });
      }
    });
  }

  void nextQuestion(){
    cancelTimer = false;
    timer = 30;
    setState(() {
      if(i < 10){
        i++;
      }
      else{
        Navigator.of(context).pop();
        Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => ResultPage(marks:marks)));
      }
      btnColor['a'] = Colors.indigoAccent;
      btnColor['b'] = Colors.indigoAccent;
      btnColor['c'] = Colors.indigoAccent;
      btnColor['d'] = Colors.indigoAccent;
    });
    startTimer();
  }

  void checkAnswer(String k){
    if(myData[2][i.toString()] == myData[1][i.toString()][k]){
      marks = marks + 5;
      colorToShow = right;
    }
    else{
      colorToShow = wrong;
    }
    setState(() {
      btnColor[k] = colorToShow;
      cancelTimer = true;
    });

    Timer(Duration(seconds: 1), nextQuestion);

  }

  Widget customButton(String k) {
    return Padding(
        padding: EdgeInsets.symmetric(
            vertical: ScreenUtil.getInstance().setHeight(20),
            horizontal: ScreenUtil.getInstance().setHeight(40)),
        child: MaterialButton(
          onPressed: () => checkAnswer(k),
          child: Text(
            myData[1][i.toString()][k],
            style: TextStyle(color: Colors.white),
            maxLines: 1,
          ),
          minWidth: ScreenUtil.getInstance().setHeight(450),
          height: ScreenUtil.getInstance().setHeight(100),
          color: btnColor[k],
          splashColor: Colors.indigo[700],
          shape: RoundedRectangleBorder(
            borderRadius:
                BorderRadius.circular(ScreenUtil.getInstance().setHeight(50)),
          ),
        ));
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations(
        [DeviceOrientation.portraitDown, DeviceOrientation.portraitUp]);
    return WillPopScope(
      onWillPop: () {
        return showDialog(
            context: context,
            builder: (context) => AlertDialog(
                  title: Text('Multiple Choice Qs'),
                  content:
                      Text('You cant go back from here, please finish the questions!'
                          ),
                  actions: <Widget>[
                    FlatButton(
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      child: Text('Close'),
                    )
                  ],
                ));
      },
      child: Scaffold(
          body: Column(children: <Widget>[
        Expanded(
          flex: 2,
          child: Container(
            padding: EdgeInsets.all(ScreenUtil.getInstance().setHeight(25)),
            alignment: Alignment.bottomCenter,
            child: Text(
              myData[0][i.toString()] != null ? myData[0][i.toString()] : '',
              style: TextStyle(fontSize: ScreenUtil.getInstance().setSp(40), fontWeight: FontWeight.bold),
            ),
          ),
        ),
        Expanded(
            flex: 7,
            child: Container(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  customButton('a'),
                  customButton('b'),
                  customButton('c'),
                  customButton('d'),
                ],
              ),
            )),
        Expanded(
            flex: 1,
            child: Container(
              alignment: Alignment.topCenter,
              child: Center(
                child: Text(
                  showTimer,
                  style: TextStyle(
                    fontSize: ScreenUtil.getInstance().setSp(60),
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
            )),
      ])),
    );
  }
}
