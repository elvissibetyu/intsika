import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:firebase_auth/firebase_auth.dart';

class UserProfile extends StatefulWidget {
  @override
  _UserProfileState createState() => _UserProfileState();
}

class _UserProfileState extends State<UserProfile> {
  String email = '';
  FirebaseUser user;

  void getUser() async{
  user = await FirebaseAuth.instance.currentUser();
  setState(() {
    email = user.email;
  });
  }
  @override
  void initState() {
    getUser();
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('My Profile', style: TextStyle(fontFamily: 'Caveat-Bold', fontSize: ScreenUtil.getInstance().setSp(45)),),
      ),
      body: Padding(
        padding: EdgeInsets.fromLTRB(ScreenUtil.getInstance().setHeight(70),ScreenUtil.getInstance().setHeight(60),ScreenUtil.getInstance().setHeight(70),0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Center(
              child: CircleAvatar(
                backgroundImage: AssetImage('assets/instika_logo_circular.PNG'),
                radius: ScreenUtil.getInstance().setHeight(90),
              ),
            ),
            Divider(
              height: ScreenUtil.getInstance().setHeight(100),
              color: Colors.black,
            ),
            Text(
              'NAME',
              style: TextStyle(
                letterSpacing: 2.0,
                color: Colors.grey,
              ),
            ),
            SizedBox(height: 7.0,),
            Text(
              user.displayName != null  ? user.displayName : 'Full Name',
              style: TextStyle(
                letterSpacing: 2.0,
                color: Colors.blue[900],
                fontSize: 28.0,
                fontWeight: FontWeight.bold,
              ),
            ),
            SizedBox(height: 30.0,),
            Text(
              'EMAIL',
              style: TextStyle(
                letterSpacing: 2.0,
                color: Colors.grey,
              ),
            ),
            SizedBox(height: 7.0,),
            Row(
              children: <Widget>[
                Icon(
                  Icons.email,
                  color: Colors.blue[900],
                ),
                SizedBox(width: 5.0),
                Expanded(
                  child: Text(
                    '$email',
                    style: TextStyle(
                      letterSpacing: 1.0,
                      fontSize: 15.0,
                      color: Colors.blue[900],
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}

