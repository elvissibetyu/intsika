import 'package:flutter/material.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class NotificationsPage extends StatefulWidget{
  @override
  _NotificationsPageState createState() => _NotificationsPageState();
}

class _NotificationsPageState extends State<NotificationsPage>{

  String textValue = 'Hello World! ';
  FirebaseMessaging firebaseMessaging = FirebaseMessaging();

  @override
  void initState() {
    firebaseMessaging.configure(
      onLaunch: (Map<String, dynamic> msg) {
        return;
    },
      onResume: (Map<String, dynamic> msg) {
        return;
      },
      onMessage: (Map<String, dynamic> msg) {
        return;
      },
    );
    firebaseMessaging.requestNotificationPermissions( const IosNotificationSettings(
      alert: true,
      badge: true,
      sound: true,
    ));
    firebaseMessaging.onIosSettingsRegistered.listen((IosNotificationSettings setting){
    });

    firebaseMessaging.getToken().then((token){
      update(token);
    });
    super.initState();
  }

  update(String token){
    textValue = token;
    setState(() {
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Notifications', style: TextStyle(fontFamily: 'Caveat-Bold', fontSize: ScreenUtil.getInstance().setSp(45)), ),
      ),
      );
  }
}