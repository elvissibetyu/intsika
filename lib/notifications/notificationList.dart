import 'package:firebase_database/firebase_database.dart';
import 'package:intsika/notifications/notification_list_item.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class MakeCall{
  List<NotificationDetailListItem> listItems = [];

  Future<List<NotificationDetailListItem>> firebaseCalls( DatabaseReference databaseReference) async {
    NotificationList notificationList;
    DataSnapshot dataSnapshot = await databaseReference.once();
    Map<dynamic, dynamic> jsonResponse = dataSnapshot.value[0]['content'];
    notificationList = new NotificationList.fromJSON(jsonResponse);
    listItems.addAll(notificationList.notificationList);
    return listItems;
  }
}
 class NotificationItems extends StatefulWidget {
   @override
   _NotificationItemsState createState() => _NotificationItemsState();
 }

 class _NotificationItemsState extends State<NotificationItems> {
  final databaseReference = FirebaseDatabase.instance.reference();
  final MakeCall makeCall = MakeCall();

   @override
   Widget build(BuildContext context) {
     var futureBuilder = new FutureBuilder(
       future: makeCall.firebaseCalls(databaseReference), // async work
       builder: (BuildContext context, AsyncSnapshot snapshot) {
         switch (snapshot.connectionState) {
           case ConnectionState.none: return Center(child: new Text('Try again when you have data/Wi-Fi'));
           case ConnectionState.waiting: return new Center(
             child: CircularProgressIndicator(),
           );
           default:
             if (snapshot.hasError)
               return new Text('Error: ${snapshot.error}');
             else
               return ListView.builder(

                 itemCount: snapshot.data.length,
                 itemBuilder: (BuildContext context, int index){
                   return Card(
                     elevation: 0.0,
                     child: Padding(
                       padding: const EdgeInsets.all(0.0),
                       child: ListTile(
                         leading: Icon(Icons.fiber_manual_record, size: 15, color: Colors.blueAccent,),
                         trailing: Icon(Icons.notifications, color: Colors.red,),
                         title: Text(snapshot.data[index].title),
                         subtitle: Text(snapshot.data[index].body),
                       ),
                     ),
                   );
                 },
               );
         }
       },
     );
     return Scaffold(
       resizeToAvoidBottomPadding: false,
       appBar: AppBar(
         title: Text(
             'Notifications',
           style: TextStyle(
             fontFamily: 'Caveat-Bold',
             fontSize: ScreenUtil.getInstance().setSp(45)
           ),
         ),
       ),
       body: futureBuilder,
     );
   }
 }





