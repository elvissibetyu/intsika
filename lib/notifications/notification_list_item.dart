class NotificationDetailListItem{
  String title;
  String body;

  NotificationDetailListItem({this.title, this.body});

  factory NotificationDetailListItem.fromJSON(Map<dynamic, dynamic> parsedJSON){
    return NotificationDetailListItem(title: parsedJSON["title"], body: parsedJSON["body"]);
  }

}

class NotificationList{
  List<NotificationDetailListItem> notificationList;

  NotificationList({this.notificationList});

  factory NotificationList.fromJSON(Map<dynamic, dynamic> json){
    return NotificationList(notificationList: parseNotifications(json));
  }

  static List<NotificationDetailListItem> parseNotifications(notificationJSON){
    var rList = notificationJSON['notifications'] as List;
    List<NotificationDetailListItem> notificationList = rList.map((data) => NotificationDetailListItem.fromJSON(data)).toList();
    return notificationList;
  }
}