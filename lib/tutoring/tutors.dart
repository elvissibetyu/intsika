import 'package:flutter/material.dart';
import 'package:slimy_card/slimy_card.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

void main() => runApp(MaterialApp(
      home: TutorList(),
    ));

class TutorList extends StatefulWidget {
  @override
  _TutorListState createState() => _TutorListState();
}

class _TutorListState extends State<TutorList> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 15.0,
        //backgroundColor: Colors.white,
        title: Text(
          'Contact Tutors Yourself',
          style: TextStyle(
              fontFamily: 'Caveat-Bold', fontSize: ScreenUtil.getInstance().setSp(45), color: Colors.white),
        ),
        centerTitle: true,
      ),
      body: Container(
        padding: EdgeInsets.only(top: 30.0),
        child: ListView(
          //scrollDirection: Axis.horizontal,
          children: <Widget>[
            SlimyCard(
              color: Colors.blueAccent,
              slimeEnabled: true,
              width: ScreenUtil.getInstance().setHeight(550),
              topCardHeight: ScreenUtil.getInstance().setHeight(450),
              bottomCardHeight: ScreenUtil.getInstance().setHeight(250),
              borderRadius: 15,
              topCardWidget: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  CircleAvatar(
                    radius: 50.0,
                    backgroundImage:
                        AssetImage('images/IMG-20191222-WA0003.jpg'),
                  ),
                  SizedBox(
                    height: ScreenUtil.getInstance().setHeight(10),
                  ),
                  Text(
                    'RNH Tutoring Academy',
                    style: TextStyle(
                        color: Colors.white,
                        wordSpacing: 2.0,
                        fontWeight: FontWeight.bold),
                  ),
                  Text(
                    'Maths & Maths Lit',
                    style: TextStyle(
                        color: Colors.white,
                        wordSpacing: 2.0,
                        fontWeight: FontWeight.bold),
                  )
                ],
              ),
              bottomCardWidget: Column(
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.only(
                        top: ScreenUtil.getInstance().setHeight(20)),
                  ),
                  Text(
                    '0671736861',
                    style: TextStyle(
                        color: Colors.white,
                        wordSpacing: 2.0,
                        fontWeight: FontWeight.bold),
                  ),
                  Text(
                    'reachingnewheightsacademy@gmail.com',
                    style: TextStyle(
                        color: Colors.white,
                        wordSpacing: 2.0,
                        fontWeight: FontWeight.bold,
                        fontSize: ScreenUtil.getInstance().setSp(25)),
                  ),
                  Text(
                    'Cape Town',
                    style: TextStyle(
                        color: Colors.white,
                        wordSpacing: 2.0,
                        fontWeight: FontWeight.bold),
                  ),
                ],
              ),
            ),
            SizedBox(height: ScreenUtil.getInstance().setHeight(20),),
            SlimyCard(
              color: Colors.blue[900],
              slimeEnabled: true,
              width: ScreenUtil.getInstance().setHeight(550),
              topCardHeight: ScreenUtil.getInstance().setHeight(450),
              bottomCardHeight: ScreenUtil.getInstance().setHeight(250),
              borderRadius: 15,
              topCardWidget: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  CircleAvatar(
                    radius: 50.0,
                    backgroundImage:
                    AssetImage('images/IMG-20191223-WA0038.jpg'),
                  ),
                  SizedBox(
                    height: ScreenUtil.getInstance().setHeight(10),
                  ),
                  Text(
                    'Fearless Minds',
                    style: TextStyle(
                        color: Colors.white,
                        wordSpacing: 2.0,
                        fontWeight: FontWeight.bold),
                  ),
                  Text(
                    'Mentorship Programme',
                    style: TextStyle(
                        color: Colors.white,
                        wordSpacing: 2.0,
                        fontWeight: FontWeight.bold),
                  )
                ],
              ),
              bottomCardWidget: Column(
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.only(
                        top: ScreenUtil.getInstance().setHeight(20)),
                  ),
                  Text(
                    '0782662193',
                    style: TextStyle(
                        color: Colors.white,
                        wordSpacing: 2.0,
                        fontWeight: FontWeight.bold),
                  ),
                  Text(
                    'fearlessminds14@gmail.com',
                    style: TextStyle(
                        color: Colors.white,
                        wordSpacing: 2.0,
                        fontWeight: FontWeight.bold,
                        fontSize: ScreenUtil.getInstance().setSp(25)),
                  ),
                  Text(
                    'Cape Town',
                    style: TextStyle(
                        color: Colors.white,
                        wordSpacing: 2.0,
                        fontWeight: FontWeight.bold),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: ScreenUtil.getInstance().setHeight(20),
            ),
            SlimyCard(
              color: Colors.lime,
              slimeEnabled: true,
              width: ScreenUtil.getInstance().setHeight(550),
              topCardHeight: ScreenUtil.getInstance().setHeight(450),
              bottomCardHeight: ScreenUtil.getInstance().setHeight(250),
              borderRadius: 15,
              topCardWidget: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  CircleAvatar(
                    radius: 50.0,
                    backgroundImage:
                        AssetImage('images/IMG-20191221-WA0002.jpg'),
                  ),
                  SizedBox(
                    height: ScreenUtil.getInstance().setHeight(10),
                  ),
                  Text(
                    'Mpumelelo Gqweta',
                    style: TextStyle(
                        color: Colors.white,
                        wordSpacing: 2.0,
                        fontWeight: FontWeight.bold,
                        fontSize: ScreenUtil.getInstance().setSp(35)),
                  ),
                  Text(
                    'Subjects: Maths & Physics',
                    style: TextStyle(
                        color: Colors.white,
                        wordSpacing: 2.0,
                        fontWeight: FontWeight.bold,
                        fontSize: ScreenUtil.getInstance().setSp(35)),
                  )
                ],
              ),
              bottomCardWidget: Column(
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.only(
                        top: ScreenUtil.getInstance().setHeight(20)),
                  ),
                  Text(
                    '0679004549',
                    style: TextStyle(
                        color: Colors.white,
                        wordSpacing: 2.0,
                        fontWeight: FontWeight.bold),
                  ),
                  Text(
                    'gqwmpu001@myuct.ac.za',
                    style: TextStyle(
                        color: Colors.white,
                        wordSpacing: 2.0,
                        fontWeight: FontWeight.bold,
                        fontSize: ScreenUtil.getInstance().setSp(35)),
                  ),
                  Text(
                    'Khayelitsha & Mowbray',
                    style: TextStyle(
                        color: Colors.white,
                        wordSpacing: 2.0,
                        fontWeight: FontWeight.bold),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: ScreenUtil.getInstance().setHeight(20),
            ),
            SlimyCard(
              color: Colors.green[500],
              slimeEnabled: true,
              width: ScreenUtil.getInstance().setHeight(550),
              topCardHeight: ScreenUtil.getInstance().setHeight(450),
              bottomCardHeight: ScreenUtil.getInstance().setHeight(250),
              borderRadius: 15,
              topCardWidget: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  CircleAvatar(
                    radius: 50.0,
                    backgroundImage:
                        AssetImage('images/FB_IMG_1569791342910.jpg'),
                  ),
                  SizedBox(
                    height: ScreenUtil.getInstance().setHeight(10),
                  ),
                  Text(
                    'Elvis Msingathi Sibetyu',
                    style: TextStyle(
                        color: Colors.white,
                        wordSpacing: 2.0,
                        fontWeight: FontWeight.bold,
                        fontSize: ScreenUtil.getInstance().setSp(35)),
                  ),
                  Text(
                    'Subjects: Maths & IT',
                    style: TextStyle(
                        color: Colors.white,
                        wordSpacing: 2.0,
                        fontWeight: FontWeight.bold),
                  )
                ],
              ),
              bottomCardWidget: Column(
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.only(
                        top: ScreenUtil.getInstance().setHeight(20)),
                  ),
                  Text(
                    '0810370591',
                    style: TextStyle(
                        color: Colors.white,
                        wordSpacing: 2.0,
                        fontWeight: FontWeight.bold),
                  ),
                  Text(
                    'elvissibetyu@gmail.com',
                    style: TextStyle(
                        color: Colors.white,
                        wordSpacing: 2.0,
                        fontWeight: FontWeight.bold,
                        fontSize: ScreenUtil.getInstance().setSp(35)),
                  ),
                  Text(
                    'Khayelitsha',
                    style: TextStyle(
                        color: Colors.white,
                        wordSpacing: 2.0,
                        fontWeight: FontWeight.bold),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
