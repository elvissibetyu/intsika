import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

void main() => runApp(MaterialApp(
    home: AboutApp(),
    ));

class AboutApp extends StatefulWidget {
  @override
  _AboutAppState createState() => _AboutAppState();
}

class _AboutAppState extends State<AboutApp> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('About Intsika App', style: TextStyle(fontFamily: 'Caveat-Bold', color: Colors.black, fontSize: ScreenUtil.getInstance().setSp(45))),
        centerTitle: true,
        elevation: 0,
        backgroundColor: Colors.white,
      ),
      body: Container(
        margin: EdgeInsets.all(10),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text('Created by Intsika Online Academy, 2019'),
            SizedBox(height: 10,),
            Text('Contact: Elvis Sibetyu @ elvissibetyu@gmail.com or 0810370591'),
            SizedBox(height: 10,),
            Text('Most of the resouces used are extracted from the South African Basic Education governments sites, you can also find the resources on their sites.'),
            SizedBox(height: 10,),
            Text('Papers from SHARP (Maths) were used.'),
            SizedBox(height:10),
            Text('Special thanks to Mpumelelo Gqweta and others for their valuable support and contributions')
          ],
        ),
      ),
    );
  }
}
